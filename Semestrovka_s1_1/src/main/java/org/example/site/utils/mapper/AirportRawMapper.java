package org.example.site.utils.mapper;

import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.enums.Country;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class AirportRawMapper implements RowMapper<Airport> {

    @Override
    public Airport from(ResultSet rs) throws SQLException {
        return Airport.builder()
                .uuid(UUID.fromString(rs.getString(1)))
                .name(rs.getString(2))
                .country(Country.valueOf(rs.getString(3)))
                .city(Country.City.valueOf(rs.getString(4)))
                .build();
    }

}
