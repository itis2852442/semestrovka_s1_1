package org.example.site.utils.todelete.dto.router;


import java.sql.Time;
import java.util.Date;
import java.util.UUID;

public class AddFlight {

    private UUID siteWorkerToken;
    private UUID routeUUID;
    private String aircraftOnBoardNumber;
    private Date departureDate;
    private Time departureTime;

}
