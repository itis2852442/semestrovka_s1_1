/*
package org.example.site.userPartOfSite.dto.request.siteUser.unAuthorized;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;
import org.example.site.utils.enums.Country;
import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
public class RegisterSiteCustomerDTO {

    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    private String patronymic;
    @NonNull
    private Date dateOfBirth;
    @NonNull
    private String phoneNumber;
    @NonNull
    private String email;
    @NonNull
    private String password;
    @NonNull
    private Country passportCountry;
    @NonNull
    private String passportDigits;

    @Builder
    public RegisterSiteCustomerDTO(@NonNull String firstName,
                                   @NonNull String lastName,
                                   @NonNull String patronymic,
                                   @NonNull Date dateOfBirth,
                                   @NonNull String phoneNumber,
                                   @NonNull String email,
                                   @NonNull String password,
                                   @NonNull Country passportCountry,
                                   @NonNull String passportDigits) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.passportCountry = passportCountry;
        this.passportDigits = passportDigits;
    }


    public SiteCustomer toEntity(){
        SiteCustomer siteCustomer = SiteCustomer.builder()
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .dateOfBirth(dateOfBirth)
                .phoneNumber(phoneNumber)
                .email(email)
                .password(password)
                .passport(Objects.requireNonNull(PasswordFactory.create(passportCountry, passportDigits)))
                .build();
        return siteCustomer;
    }
}
*/
