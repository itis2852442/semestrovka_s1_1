package org.example.site.utils.todelete.dto2.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.example.site.userPartOfSite.models.flights.Ticket;

@Getter
@Setter
public class BuyTicketResponse {

    private boolean bought;
    private Ticket ticket;

    @Builder
    public BuyTicketResponse(boolean bought, Ticket ticket) {
        this.bought = bought;
        this.ticket = ticket;
    }
}
