package org.example.site.utils.todelete.dto.superr;

import org.example.site.utils.enums.SiteUserRole;

import java.util.Date;
import java.util.UUID;

public class RegisterSiteWorker {

    private UUID SiteWorkerToken;
    private String firstName;
    private String lastName;
    private String patronymic;
    private Date dateOfBirth;
    private String phoneNumber;
    private String email;
    private String password;
    private SiteUserRole role;

}
