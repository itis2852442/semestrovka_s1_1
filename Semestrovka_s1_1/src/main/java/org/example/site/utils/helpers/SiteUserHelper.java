package org.example.site.utils.helpers;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.site.utils.context.MyContext;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Log4j2
public class SiteUserHelper {

    public static Optional<SiteCustomer> findSiteUserCustomerFromRequest(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        Optional<SiteCustomer> siteCustomerOptional = Optional.empty();
        SiteCustomer siteCustomer = null;

        Optional<Cookie> cookieTokenOptional = Helper.findCookieWithName(cookies, "token");

        if (cookieTokenOptional.isPresent()) {
            log.debug("получен token из cookies");
            siteCustomerOptional = MyContext.siteUserCustomerCRUDRepository.find(cookieTokenOptional.get().getValue());
            if (siteCustomerOptional.isPresent()) {
                siteCustomer = siteCustomerOptional.get();
                log.debug("получен site_customer c помощью token");
            }
        } else {
            log.debug("there is no token in cookies");
        }
        return Optional.ofNullable(siteCustomer);
    }


    public static Optional<SiteWorker> findSiteWorkerFromRequest(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        Optional<SiteWorker> siteWorkerOptional = Optional.empty();
        SiteWorker siteWorker = null;

        Optional<Cookie> cookieTokenOptional = Helper.findCookieWithName(cookies, "token");

        if (cookieTokenOptional.isPresent()) {
            log.debug("получен token из cookies");
            siteWorkerOptional = MyContext.siteWorkerCRUDRepository.find(cookieTokenOptional.get().getValue());
            if (siteWorkerOptional.isPresent()) {
                siteWorker = siteWorkerOptional.get();
                log.debug("получен site_worker c помощью token");
            }
        } else {
            log.debug("there is no token in cookies");
        }
        return Optional.ofNullable(siteWorker);
    }

    public static Optional<SiteUser> findSiteUserOptByReq(HttpServletRequest req){
        Optional<Cookie> cookieWithTokenOpt =  Helper.findCookieWithName(req.getCookies(), "token");
        if(cookieWithTokenOpt.isPresent()){
            String token = cookieWithTokenOpt.get().getValue();
            return MyContext.siteUserCRUDRepo.find(token);
        }
        return Optional.empty();
    }

    ////


    public static Optional<SiteUser> findOptFrom2Opt(Optional<SiteCustomer> siteUserCustomerOptional, Optional<SiteWorker> siteWorkerOptional, String mess) {
        if (siteUserCustomerOptional.isPresent() && siteWorkerOptional.isPresent()) {
            throw new RuntimeException(mess);
        }

        Optional<SiteUser> siteUserOpt = Optional.empty();
        if (siteUserCustomerOptional.isPresent()) {
            siteUserOpt = Optional.of(siteUserCustomerOptional.get());
        }
        if (siteWorkerOptional.isPresent()) {
            siteUserOpt = Optional.of(siteWorkerOptional.get());
        }
        return siteUserOpt;
    }









}
