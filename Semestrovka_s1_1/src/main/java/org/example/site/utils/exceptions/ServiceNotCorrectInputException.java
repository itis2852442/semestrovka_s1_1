package org.example.site.utils.exceptions;

public class ServiceNotCorrectInputException extends ServiceException {
    public ServiceNotCorrectInputException(String message) {
        super(message);
    }

    public ServiceNotCorrectInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
