//package org.example.site.utils.freemarker;
//
//import javax.servlet.*;
//import java.io.IOException;
//
///**
// * This simple filter adds the HttpServletRequest object to the Request Attributes with the key "RequestObject"
// * so that it can be referenced from Freemarker.
// */
//public class RequestObjectAttributeFilter implements Filter {
//
//    public void init(FilterConfig paramFilterConfig) throws ServletException {
//
//    }
//
//    public void doFilter(ServletRequest req,
//                         ServletResponse res, FilterChain filterChain)
//            throws IOException, ServletException {
//        req.setAttribute("RequestObject", req);
//
//        filterChain.doFilter(req, res);
//    }
//
//    public void destroy() {
//
//    }
//
//}
