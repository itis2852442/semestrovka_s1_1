package org.example.site.utils.todelete.dto.superr;

import java.util.Date;
import java.util.UUID;

public class AddAircraft {

    private UUID SiteWorkerToken;
    private String OnBoardNumber;

    private String distro;
    private String model;
    private Date releaseYear;

    private int maxCapacity;
    private int amountOfSeats;
    private String schema;

}
