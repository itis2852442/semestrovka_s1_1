package org.example.site.utils.helpers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Optional;

public class Helper extends HttpServlet {

    public static void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


    public static Optional<Cookie> findCookieWithName(Cookie[] cookies, String name) {
        if (cookies == null) {
            return Optional.empty();
        }
        return Arrays.stream(cookies)
                .filter(x -> x.getName().equals(name))
                .findFirst();
    }


}
