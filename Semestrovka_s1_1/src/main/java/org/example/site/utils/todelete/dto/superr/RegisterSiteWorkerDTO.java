/*
package org.example.site.infrastructurePartOfSite.dto.superr;

import lombok.Builder;
import lombok.NonNull;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.enums.Country;
import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;

import java.util.Date;
import java.util.Objects;

public class RegisterSiteWorkerDTO {

    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    private String patronymic;
    @NonNull
    private Date dateOfBirth;
    @NonNull
    private String phoneNumber;
    @NonNull
    private String email;
    @NonNull
    private String password;
    @NonNull
    private Gender gender;
    @NonNull
    private SiteUserRole role;

    @Builder
    public RegisterSiteWorkerDTO(@NonNull String firstName,
                                 @NonNull String lastName,
                                   @NonNull String patronymic,
                                   @NonNull Date dateOfBirth,
                                   @NonNull String phoneNumber,
                                   @NonNull String email,
                                   @NonNull String password,
                                   @NonNull Gender gender,
                                   @NonNull SiteUserRole role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.gender=gender;
        this.role=role;
    }


    public SiteWorker toEntity(){
        SiteWorker siteWorker = SiteWorker.builder()
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .age(age)
                .phoneNumber(phoneNumber)
                .gender(gender)
                .email(email)
                .password(password)
                .role(role)
                .build();
        return siteWorker;
    }

}
*/
