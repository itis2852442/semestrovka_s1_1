package org.example.site.utils.enums;

public enum SiteUserRole {

    SIMPLE_CUSTOMER,
    ADMIN,
    ROUTER,
    SUPER

}
