package org.example.site.utils.context;

//import org.example.repos.userRepos.ActiveSeancesRepo;

import lombok.extern.log4j.Log4j2;
import org.example.site.infrastructurePartOfSite.repo.*;
import org.example.site.infrastructurePartOfSite.services.*;
import org.example.site.userPartOfSite.repos.userRepos.impl.SiteUserHelperRepo;
import org.example.site.userPartOfSite.repos.userRepos.impl.SiteCustomerRepositoryOnJDBCImpl;
import org.example.site.userPartOfSite.repos.userRepos.impl.SiteWorkerRepositoryOnJDBCImpl;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteCustomerRepository;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteWorkerRepository;
import org.example.site.userPartOfSite.services.FlightService;
import org.example.site.userPartOfSite.services.TicketsService;
import org.example.site.userPartOfSite.services.users.SiteCustomerService;
import org.example.site.userPartOfSite.services.users.sec.SiteCustomerSecurityService;
import org.example.site.userPartOfSite.services.users.sec.SiteWorkerSecurityService;
import org.example.site.utils.conf.DBConf;
import org.example.site.utils.service.CheckerService;

import javax.servlet.http.HttpServlet;

@Log4j2
public class MyContext extends HttpServlet {

    //    public static Properties locations = new Properties();;
    public static CheckerService checkerService = new CheckerService();
//    public static SiteUserSecurityService siteUserSecurityService = new SiteUserSecurityService();

//    {
//        try {
//            log.debug("init location properties: ");
//            locations.load(new FileInputStream("D:\\git_lab_folder\\itis\\oris\\Semestrovka_s1_1_repo\\Semestrovka_s1_1\\src\\main\\resources\\locations.properties"));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

    //public static VoteRepository voteRepository = new VoteRepository(url, user, password);
    public static SiteCustomerRepository siteUserCustomerCRUDRepository = new SiteCustomerRepositoryOnJDBCImpl(DBConf.getUrl(), DBConf.getUser(), DBConf.getPassword());
    public static SiteWorkerRepository siteWorkerCRUDRepository = new SiteWorkerRepositoryOnJDBCImpl(DBConf.getUrl(), DBConf.getUser(), DBConf.getPassword());
    public static SiteUserHelperRepo siteUserCRUDRepo = new SiteUserHelperRepo();

    //public static ActiveSeancesRepo activeSeancesRepo = new ActiveSeancesRepo(url, user, password);

    public static SiteCustomerService siteCustomerService = new SiteCustomerService();
    public static FlightService flightService = new FlightService();
    public static TicketsService ticketsService = new TicketsService();

    //sec
    public static SiteWorkerSecurityService siteWorkerSecurityService = new SiteWorkerSecurityService();
    public static SiteCustomerSecurityService siteCustomerSecurityService = new SiteCustomerSecurityService();


    //infr
    public static TicketInfrRepoJDBCImpl ticketInfrRepoJDBCImpl = new TicketInfrRepoJDBCImpl(DBConf.getUrl(), DBConf.getUser(), DBConf.getPassword());
    public static FlightInfrService FlightInfrService = new FlightInfrService();
    public static FlightInfrRepoJDBCImpl flightInfrRepoJDBCImpl = new FlightInfrRepoJDBCImpl(DBConf.getUrl(), DBConf.getUser(), DBConf.getPassword());
    public static TicketInfrService ticketInfrService = new TicketInfrService();

    public static AirportRepoJDBCImpl airportRepoJDBC = new AirportRepoJDBCImpl(DBConf.getUrl(),DBConf.getUser(),DBConf.getPassword());
    public static AirportInfrService airportInfrService = new AirportInfrService();
    public static AircraftRepoJDBCImpl aircraftRepoJDBC = new AircraftRepoJDBCImpl(DBConf.getUrl(),DBConf.getUser(),DBConf.getPassword());
    public static AircraftInfrService aircraftInfrService = new AircraftInfrService();
    public static PathRepoJDBCImpl pathRepoJDBC = new PathRepoJDBCImpl(DBConf.getUrl(),DBConf.getUser(),DBConf.getPassword());
    public static PathInfrService pathInfrService = new PathInfrService();

    public static CountryCityRepoJDBCImpl countryCityRepoJDBCImpl = new CountryCityRepoJDBCImpl(DBConf.getUrl(),DBConf.getUser(),DBConf.getPassword());
    public static CountryCityInfrService countryCityInfrService = new CountryCityInfrService();
    ////////////////////////////////////////////////////////////////////////


    //public static VoteService voteService = new VoteService();

    /*public static CheckerService checkerService;

    //public static VoteRepository voteRepository = new VoteRepository(url, user, password);
    public static SiteCustomerRepository siteUserCustomerCRUDRepository;
    public static SiteWorkerRepository siteWorkerCRUDRepository;
    public static SiteUserHelperRepo siteUserCRUDRepo;

    //public static ActiveSeancesRepo activeSeancesRepo = new ActiveSeancesRepo(url, user, password);

    public static SiteCustomerService siteCustomerService;
    public static FlightService flightService;
    public static TicketsService ticketsService;

    //sec
    public static SiteWorkerSecurityService siteWorkerSecurityService;
    public static SiteCustomerSecurityService siteCustomerSecurityService;


    //infr
    public static TicketInfrRepoJDBCImpl ticketInfrRepoJDBCImpl;
    public static FlightInfrService FlightInfrService;
    public static FlightInfrRepoJDBCImpl flightInfrRepoJDBCImpl;
    public static TicketInfrService ticketInfrService;*/


    //    public static Properties locations = new Properties();;
//    public static SiteUserSecurityService siteUserSecurityService = new SiteUserSecurityService();

//    {
//        try {
//            log.debug("init location properties: ");
//            locations.load(new FileInputStream("D:\\git_lab_folder\\itis\\oris\\Semestrovka_s1_1_repo\\Semestrovka_s1_1\\src\\main\\resources\\locations.properties"));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }



    //public static VoteService voteService = new VoteService();





}
