package org.example.site.utils.freemarker;

import freemarker.template.Template;

import java.io.IOException;

public class FreemarkerTemplates {

    public static Template getUnAuthorizedHome(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/customer/home/homeUnauthorized.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    ///

    public static Template getSimpleCustomerHome(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/customer/home/homeSimpleCustomer.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getCustomerLogin(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/customer/login/loginCustomer.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getCustomerReg(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/customer/reg/regCustomer.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    ///

    public static Template getWorkerLogin(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/login/infrLogin.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getWorkerReg(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/regSiteWorker.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    ///

    public static Template getAdminHome(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/adming/homeAdmin.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getRouterHome(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/router/homeRouter.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getSuperrHome(){
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/homeSuperr.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getSuperrAddAirport() {
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/addAirportSuperr.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getSuperrAddAircraft() {
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/addAircraftSuperr.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Template getSuperrAddPath() {
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/addPathSuperr.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static Template getSuperrAddCountryCity() {
        try {
            return FreemarkerUtil.getConfig().getTemplate("/infr/superr/addCountryCitySuperr.ftl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }


//    public static Template getSiteUserLogout(){
//
//    }

}
