package org.example.site.utils.service;

import lombok.extern.log4j.Log4j2;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceException;

import javax.servlet.http.Cookie;

@Log4j2
public class CheckerService {


    public boolean checkSiteCustomerReg(String firstName,
                                        String lastName,
                                        String patronymic,
                                        String age,
                                        String phoneNumber,
                                        String email,
                                        String password,
                                        String passportCountry,
                                        String passportDigits) throws Exception {

        if(
                firstName==null || lastName==null || patronymic==null || age==null || phoneNumber==null
                || email==null || password==null || passportCountry==null || passportDigits==null
        ){
            log.debug("\n" +
                    "firstName: " +firstName+ "\n" +
                    " lastName: " +lastName+"\n" +
                    " patronymic: " + patronymic+"\n" +
                    " age: " +age+"\n" +
                    " phoneNumber: " +phoneNumber+"\n" +
                    " email: " +email+"\n" +
                    " password: " +password+"\n" +
                    " passportCountry: " +passportCountry+"\n" +
                    " passportDigits: " +passportDigits);
            throw new ServiceException("some field is null");
        }

        if(firstName.length()<1 || lastName.length()<1 || patronymic.length()<1){
            log.debug("firstName "+firstName);
            log.debug("lastName "+lastName);
            log.debug("patronymic "+patronymic);
            throw new ServiceException("len of names < 1");
        }

        try{
            Integer.parseInt(age);
        } catch (NumberFormatException e){
            log.debug("parseInt " + age);
            throw new ServiceException("parseInt " + age);
        }
        try{
            Integer.parseInt(phoneNumber);
        } catch (NumberFormatException e){
            log.debug("parseInt " + phoneNumber);
            throw new ServiceException("parseInt " + phoneNumber);
        }



        if(!email.contains("@")){
            log.debug("email " + email);
            throw new ServiceException("email " + email);
        }

        if(password.length()<8){
            log.debug("password " + password);
            throw new ServiceException("password " + password);
        }

        try{
            Country.valueOf(passportCountry);
        } catch (Exception e){
            log.debug("Country " + passportCountry );
            throw new ServiceException("Country " + passportCountry);
        }

        if(PasswordFactory.create(Country.valueOf(passportCountry), passportDigits) ==null){
            log.debug("creating password " + passportDigits);
            throw new ServiceException("creating password " + passportDigits);
        }

        return true;

    }


    public boolean checkSiteWorkerReg(String firstName, String lastName, String patronymic, String age, String phoneNumber, String email, String password, String role) {
        //todo
        return false;

    }
}
