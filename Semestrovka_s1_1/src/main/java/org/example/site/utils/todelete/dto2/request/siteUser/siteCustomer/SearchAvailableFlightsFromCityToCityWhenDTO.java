package org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.example.site.utils.enums.Country;

import java.util.Date;

@Getter
@Setter
public class SearchAvailableFlightsFromCityToCityWhenDTO {

    @NonNull
    private Country.City fromCity;
    @NonNull
    private Country.City toCity;
    @NonNull
    private Date dateStart;
    @NonNull
    private Date dateEnd;

    @Builder
    public SearchAvailableFlightsFromCityToCityWhenDTO(@NonNull Country.City fromCity,
                                                       @NonNull Country.City toCity,
                                                       @NonNull Date dateStart,
                                                       @NonNull Date dateEnd) {
        this.fromCity = fromCity;
        this.toCity = toCity;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
}
