package org.example.site.utils.exceptions;

public class ServiceConflictException extends ServiceException {
    public ServiceConflictException(String message) {
        super(message);
    }

    public ServiceConflictException(String message, Throwable cause) {
        super(message, cause);
    }
}
