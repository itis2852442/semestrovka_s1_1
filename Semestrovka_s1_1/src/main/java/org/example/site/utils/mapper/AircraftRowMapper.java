package org.example.site.utils.mapper;

import org.example.site.infrastructurePartOfSite.models.Aircraft;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.enums.AircraftDistro;
import org.example.site.utils.enums.AircraftModel;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AircraftRowMapper implements RowMapper<Aircraft> {
    @Override
    public Aircraft from(ResultSet rs) throws SQLException {
        Aircraft aircraft = Aircraft.builder()
                .OnBoardNumber(rs.getString(1))
                .distro(AircraftDistro.valueOf(rs.getString(2)))
                .model(AircraftModel.valueOf(rs.getString(3)))
                .age(rs.getInt(4))
                .maxCapacity(rs.getInt(5))
                .amountOfSeats(rs.getInt(6))
                .build();
        return aircraft;
    }
}
