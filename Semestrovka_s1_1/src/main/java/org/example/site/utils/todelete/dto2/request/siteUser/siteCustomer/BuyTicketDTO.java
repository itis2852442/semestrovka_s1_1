package org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class BuyTicketDTO {

    @NonNull
    private UUID flightUUID;
    private int place;

    @Builder
    public BuyTicketDTO(@NonNull UUID flightUUID,
                        int place) {
        this.flightUUID = flightUUID;
        this.place = place;
    }
}
