package org.example.site.utils.locations;

//import org.example.site.utils.MyProperties;

public class Locations {

    public static String getSiteUserLogout() {
        return "/logout";
    }

//
    public static String getCustomerLogin() {
        return "/login";
    }

    public static String getCustomerReg() {
        return "/reg";
    }

    public static String getCustomerHome() {
        return "/home";
    }
//

    public static String getCustomerAddComment() {
        return "/addComment";
    }

    public static String getCustomerSearchFlight() {
        return "/searchFlight";
    }

    public static String getCustomerBuyTicket() {
        return "/buyTicket";
    }


///

    public static String getWorkerLogin() {
        return "/infr/login";
    }

    public static String getWorkerHome() {
        return "/infr/home";
    }
//

    public static String getAdminAddHeadline() {
        return "/infr/admin/addHeadline";
    }

    public static String getAdminAddComment() {
        return "/infr/admin/addComment";
    }

//
    public static String getRouterAddHeadline() {
        return "/infr/router/addFlight";
    }

//

    public static String getSuperrReg() {
        return "/infr/superr/regSiteWorker";
    }

    public static String getSuperrAddAircraftToPath() {
        return "/infr/superr/addAircraftToPath";
    }

    public static String getSuperrAddPath() {
        return "/infr/superr/addPath";
    }

    public static String getSuperrAddAirport() {
        return "/infr/superr/addAirport";
    }

    public static String getSuperrAddAircraft() {
        return "/infr/superr/addAircraft";
    }


    public static String getSuperrAddCountryCity() {

        return "/infr/superr/addCountryCity";
    }
}
