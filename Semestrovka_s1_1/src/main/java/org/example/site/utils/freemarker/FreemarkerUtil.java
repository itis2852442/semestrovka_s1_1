package org.example.site.utils.freemarker;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;

import java.io.File;
import java.io.IOException;

public class FreemarkerUtil {

    private static final Configuration configuration = bootConfig();

    private static Configuration bootConfig() {
        Configuration conf = new Configuration(Configuration.VERSION_2_3_21);
        conf.setDefaultEncoding("UTF-8");
        try {
            conf.setTemplateLoader(new FileTemplateLoader(new File("D:\\git_lab_folder\\itis\\oris\\Semestrovka_s1_1_repo\\Semestrovka_s1_1\\src\\main\\webapp\\WEB-INF\\templates\\ftl")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return conf;
    }

    public static Configuration getConfig() {
        return configuration;
    }
}
