/*
package org.example.customerPartOfSite.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public enum SiteCustomerDTO {;
    private interface Id { @Positive Long getId(); }
    private interface Name { @NotBlank String getName(); }
    private interface PhoneNumber { @NotBlank String getPhoneNumber(); }
    private interface Email { @NotBlank String getEmail(); }
*/
/*    private interface Price { @Positive Double getPrice(); }
    private interface Cost { @Positive Double getCost(); }*//*

    public enum Request{;
        @Value public static class CommonReq implements Email{
            String email;
        }
    }

    public enum Response{;
        @Value
        public static class FullResp implements Id, Name, PhoneNumber, Email{
            Long id;
            String name;
            String phoneNumber;
            String email;
        }

        @Value
        public static class CommonResp implements Name, PhoneNumber, Email{
            String name;
            String phoneNumber;
            String email;
        }
    }
}*/
