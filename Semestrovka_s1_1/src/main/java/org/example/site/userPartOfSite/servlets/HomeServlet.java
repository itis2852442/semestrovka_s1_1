package org.example.site.userPartOfSite.servlets;

//import org.example.model.Credentials;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.helpers.SiteUserHelper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "HomeServlet", urlPatterns = "/home")
public class HomeServlet extends HttpServlet {

    /*@Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        HashMap<String, Credentials> map = new HashMap<>();
        servletContext.setAttribute("task3TokenAndCredentialsMap", map);

        servletContext.setAttribute("task3AmountOfVotesBMW",0);
        servletContext.setAttribute("task3AmountOfVotesMERCEDES",0);
        servletContext.setAttribute("task3AmountOfVotesTOYOTA",0);
        servletContext.setAttribute("task3AmountOfVotesVOLVO",0);
        super.init();
    }*/


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Optional<SiteCustomer> siteCustomerOpt = SiteUserHelper.findSiteUserCustomerFromRequest(req);

        /*if (!site_customer.isVoted()) {
            Template template = FreemarkerUtil.getConfig().getTemplate("src/main/WEB-INF/templates/secretPageToNotVotedUsers.ftl");
            HashMap<String, Object> map = new HashMap<>();

            List<SiteCustomer> simpleSiteUserList = MyContext.simpleSiteUserRepository.getAll();
            map.put("siteUsers", simpleSiteUserList);

            resp.setContentType("text/html");
            try {
                template.process(map, resp.getWriter());
            } catch (TemplateException e) {
                throw new RuntimeException(e);
            }
//            resp.getWriter().println(pageContent);
            //resp.sendRedirect("secretPage.jsp");
        } else {
            Template template = FreemarkerUtil.getConfig().getTemplate("src/main/WEB-INF/templates/secretPageToVotedUsers.ftl");
            HashMap<String, Object> map = new HashMap<>();

            List<Vote> allVotes = MyContext.voteRepository.getAll();
            Map<String, Integer> voteCandidateAmountVoted = allVotes.stream()
                    .collect(Collectors.groupingBy(Vote::getCandidateName, Collectors.summingInt(e -> 1)));

            System.out.println("map:");
            System.out.println(allVotes);
            System.out.println(voteCandidateAmountVoted);

            map.put("voteCandidateAmountVoted", voteCandidateAmountVoted);

            resp.setContentType("text/html");
            try {
                template.process(map, resp.getWriter());
            } catch (TemplateException e) {
                throw new RuntimeException(e);
            }
        }*/

        if (siteCustomerOpt.isPresent()) {
            SiteCustomer siteCustomer = siteCustomerOpt.get();

            // fixme repos
            if (siteCustomer.getRole() == SiteUserRole.SIMPLE_CUSTOMER) {
                Template template = FreemarkerTemplates.getSimpleCustomerHome();
                HashMap<String, Object> map = new HashMap<>();

                List<SiteCustomer> simpleSiteUserList = MyContext.siteUserCustomerCRUDRepository.getAll();
                map.put("siteUsers", simpleSiteUserList);

                resp.setContentType("text/html");
                try {
                    template.process(map, resp.getWriter());
                } catch (TemplateException | IOException e) {
                    throw new RuntimeException(e);
                }
            }
        } else {
            Template template = FreemarkerTemplates.getUnAuthorizedHome();
            HashMap<String, Object> map = new HashMap<>();

            resp.setContentType("text/html");
            try {
                template.process(map, resp.getWriter());
            } catch (TemplateException | IOException e) {
                throw new RuntimeException(e);
            }
//            resp.getWriter().println(pageContent);
            //resp.sendRedirect("secretPage.jsp");
        }


//        System.out.println(pageContent);
//        System.out.println("home");

    }
}