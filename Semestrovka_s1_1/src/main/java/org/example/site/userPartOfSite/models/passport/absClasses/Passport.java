package org.example.site.userPartOfSite.models.passport.absClasses;

import lombok.*;
import org.example.site.utils.enums.Country;

//fixme may be different field, but children use only some of them

/**
 * passport contains only unique information;
 * for example, for Russia - seria, nomer
 */
@Getter
@Setter
public abstract class Passport {

    @NonNull
    private Country country;


    public Passport(Country country){
        this.country=country;
    }

    public abstract String getDigits();

//    public String toStringEnterpretation() {
//        return getCountry() + "/" + getDigits();
//    }

}
