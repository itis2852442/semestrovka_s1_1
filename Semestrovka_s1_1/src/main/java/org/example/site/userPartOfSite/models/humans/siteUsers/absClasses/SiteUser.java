package org.example.site.userPartOfSite.models.humans.siteUsers.absClasses;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
//import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.userPartOfSite.models.humans.Human;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class SiteUser extends Human {

    protected UUID token;
    @NonNull
    protected String email;
    @NonNull
    protected String password;
    @NonNull
    protected SiteUserRole role;
    protected boolean banned;

    public SiteUser() {
    }

    public SiteUser(@NonNull String firstName,
                    @NonNull String lastName,
                    @NonNull String patronymic,
                    int age,
                    @NonNull String phoneNumber,
                    UUID token,
                    @NonNull String email,
                    @NonNull String password,
                    @NonNull SiteUserRole role,
                    boolean banned) {
        super(firstName, lastName, patronymic, age, phoneNumber);
        this.token = token;
        this.email = email;
        this.password = password;
        this.role = role;
        this.banned = banned;
    }
}
