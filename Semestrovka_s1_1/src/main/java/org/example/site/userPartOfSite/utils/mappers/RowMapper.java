package org.example.site.userPartOfSite.utils.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface RowMapper<E> {
    E from(ResultSet rs) throws SQLException;
}
