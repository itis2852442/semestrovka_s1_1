package org.example.site.userPartOfSite.repos.userRepos.absClasses;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteWorkerRepository;
import org.example.site.userPartOfSite.utils.mappers.SiteWorkerRowMapper;

public abstract class SiteWorkerRepositoryOnJDBC extends SiteUserRepositoryOnJDBC<SiteWorker> implements SiteWorkerRepository {

    public SiteWorkerRepositoryOnJDBC(String url, String user, String password) {
        super(url, user, password);
        super.rowMapper = new SiteWorkerRowMapper();
    }

    @Override
    protected String getTABLE_NAME() {
        return "site_worker";
    }
}
