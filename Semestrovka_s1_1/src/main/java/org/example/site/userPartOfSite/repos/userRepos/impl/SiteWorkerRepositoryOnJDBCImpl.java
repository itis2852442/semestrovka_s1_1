package org.example.site.userPartOfSite.repos.userRepos.impl;


import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.repos.userRepos.absClasses.SiteWorkerRepositoryOnJDBC;
import org.example.site.utils.helpers.Helper;

import java.sql.*;
import java.util.UUID;

@Log4j2
public class SiteWorkerRepositoryOnJDBCImpl extends SiteWorkerRepositoryOnJDBC {

    //language=sql
    private static final String SQL_INSERT_SITE_USER_PREP =
            "insert into site_worker(" +
                    "name, " +
                    "surname, " +
                    "patronymic, " +
                    "age, " +
                    "password, " +
                    "banned, " +
                    "phone_number, " +
                    "email " +
                    ") values (?,?,?,?,?,?,?,?,?,?,?)";

    //language=sql
    private static final String SQL_UPDATE_WHERE_TOKEN_EQ =
            "update site_worker " +
                    "set name = ?, " +
                    "surname = ?, " +
                    "patronymic = ?, " +
                    "age = ?, " +
                    "password = ?, " +
                    "banned = ?, " +
                    "phone_number = ?, " +
                    "email = ? " +
                    "where token = ?";


    /*@Override
    public Logger getLogger() {
        return LogManager.getLogger(SiteWorkerRepositoryOnJDBCImpl.class);
    }*/

    public SiteWorkerRepositoryOnJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }


    private PreparedStatement putSiteWorkerToInsertPreparedStatement(PreparedStatement statement, SiteWorker siteWorker) throws SQLException {
        statement.setString(1, siteWorker.getFirstName());
        statement.setString(2, siteWorker.getLastName());
        statement.setString(3, siteWorker.getPatronymic());
        //fixme date
        statement.setInt(4, siteWorker.getAge());
        statement.setString(6, siteWorker.getPassword());
        statement.setBoolean(7, siteWorker.isBanned());
        statement.setString(8, siteWorker.getPhoneNumber());
        statement.setString(9, siteWorker.getEmail());
        return statement;
    }

    private PreparedStatement putSiteWorkerToUpdatePreparedStatement(PreparedStatement statement, SiteWorker siteWorker) throws SQLException {
        statement.setString(1, siteWorker.getFirstName());
        statement.setString(2, siteWorker.getLastName());
        statement.setString(3, siteWorker.getPatronymic());
        //fixme date
        statement.setInt(4, siteWorker.getAge());
        statement.setString(6, siteWorker.getPassword());
        statement.setBoolean(7, siteWorker.isBanned());
        statement.setString(8, siteWorker.getPhoneNumber());
        statement.setString(9, siteWorker.getEmail());
        return statement;
    }

    @Override
    public String add(SiteWorker siteWorker) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SITE_USER_PREP);
            putSiteWorkerToInsertPreparedStatement(statement, siteWorker);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 1) {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return (generatedKeys.getString(1));
                    }
                    else {
                        throw new SQLException("failed: no UUID");
                    }
                }
            } else if (affectedRows == 0) {
                return null;
            } else {
                throw new RuntimeException("collisiums");
            }
        } catch (SQLException e) {
//                System.out.println("###");
//                System.out.println(e.getErrorCode());
//                System.out.println("###");
//                throw new RuntimeException(e);
            Helper.printSQLException(e);
            throw new RuntimeException(e);
        }


    }




    /*public void setVotedTrueToExistingSiteUserWithAllChecks(String token) {

        Optional<SiteWorker> siteUserOptional = getSiteUserOptByToken(token);
        if (siteUserOptional.isPresent()) {
            SiteWorker siteWorker = siteUserOptional.get();

            siteWorker.setVotedTrue();
            //updating siteWorker in db, that he has voted
            int changedRowsUpdating = update(siteWorker);
            //checking
            if (changedRowsUpdating > 0) {
                Optional<SiteWorker> siteUserOptAfterUpdating = getSiteUserOptByToken(token);
                if (siteUserOptAfterUpdating.isPresent()) {
                    SiteWorker siteWorkerAfterUpdating = siteUserOptAfterUpdating.get();

                    if (!siteWorkerAfterUpdating.isVoted()) {
                        throw new RuntimeException("added vote to voteRepo, but siteWorker.isVoted==false");
                    }
                }
            } else {
                throw new RuntimeException("updating siteWorker crashed");
            }
        }
    }*/


    /*public void checkIfTokenOneAndOnlyOneInDB(String token) {
        int len = getListSiteUsersWhereFieldEq("token", token).size();
        if (len != 1) {
            throw new RuntimeException("token not OneAndOnlyOne");
        }
    }*/


    /*@Override
    public boolean isSiteUserVoted(String token) {
        List<SiteWorker> list = getListSiteUsersWhereFieldEq("token", token);
        if (list.size() == 1) {
            return list.get(0).isVoted();
        } else if (list.size() > 1) {
            throw new RuntimeException("collisiums");
        } else {
            return false;
        }
    }*/


    /*private boolean systemHasEmail(String email) {
        List<SiteWorker> list = getListSiteUsersWhereFieldEq("email", email);
        if (list.size() > 1) {
            throw new RuntimeException("collisiums");
        } else if (list.size() == 1) {
            return true;
        } else {
            return false;
        }
    }*/

    @Override
    public boolean update(SiteWorker siteWorkerToInsert) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_WHERE_TOKEN_EQ);
            putSiteWorkerToUpdatePreparedStatement(statement, siteWorkerToInsert);
            int amount = statement.executeUpdate();
            if (amount == 1) {
                return true;
            } else if (amount == 0) {
                return false;
            } else {
                throw new RuntimeException("collisiums");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Logger getLogger() {
        return log;
    }

}
