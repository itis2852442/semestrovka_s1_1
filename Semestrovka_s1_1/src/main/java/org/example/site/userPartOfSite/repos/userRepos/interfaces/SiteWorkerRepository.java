package org.example.site.userPartOfSite.repos.userRepos.interfaces;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;

public interface SiteWorkerRepository extends SiteUserRepository<SiteWorker> {
}
