/*
package org.example.site.userPartOfSite.models.humans.realLifeWorkers;

import lombok.Getter;
import lombok.Setter;
import org.example.site.utils.enums.WorkerRole;
import org.example.site.userPartOfSite.models.humans.Human;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Worker extends Human {

    private UUID uuid;
    private WorkerRole role;
    private int salary;
    private Date workingStartDate;

    public Worker() {
    }

    public Worker(String firstName, String lastName, String patronymic, Date dateOfBirth, String phoneNumber, WorkerRole role, int salary, Date workingStartDate) {
        super(firstName, lastName, patronymic, dateOfBirth, phoneNumber);
        this.role = role;
        this.salary = salary;
        this.workingStartDate = workingStartDate;
    }

}
*/
