package org.example.site.userPartOfSite.repos.userRepos.interfaces;

import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
import org.example.site.userPartOfSite.repos.CRUDRepository;

import java.util.Optional;

/*
    Todo можно было сделать так:
        в базе данных 2 таблицы: customer и site_worker.
        3 таблица: (token, role)
        но можно ли реализовать эту 3-ью таблицу, значение в строке которой(token) ссылается либо на значение в 1-ой таблице
        либо на значение во 2-ой.
        что-то вроде:
        foreign key (token) references (select token from customer, site_worker) (token);
        и вместо 2 репозиториев - SimpleSiteUserCRUDRepo и SiteWorkerCRUDRepo
        иметь только один - SiteUserHelperRepo.
        при второй реализации в каждом методе нужно было бы прописывать switch (role).
        2 способ возможно лучше.
        решение, к которому пришел: SiteUserHelperRepo.
     */
public interface SiteUserRepository<T extends SiteUser> extends CRUDRepository<T> {

    Optional<T> findSiteUserOptByEmailAndPassword(String email, String password);

    Optional<T> findSiteUserOptByEmail(String email);

    Optional<T> findSiteUserOptByPhone(String phone);


    /*boolean isSiteUserVoted(String token);

    int updateVotedToTrue(String token);*/

}
