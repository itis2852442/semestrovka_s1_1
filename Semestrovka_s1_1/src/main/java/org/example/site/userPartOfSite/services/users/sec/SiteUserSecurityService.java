package org.example.site.userPartOfSite.services.users.sec;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
import org.example.site.utils.context.MyContext;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

public abstract class SiteUserSecurityService {

    protected Integer TIME_COOKIE_WITH_TOKEN_LIFE_WITHOUT_REMEMBER_ME;
    protected Integer TIME_COOKIE_WITH_TOKEN_LIFE_WITH_REMEMBER_ME;

    protected final Logger log = getLogger();

    //todo оставил этот метод здесь, для того случая если и siteWorker и siteCustomer будут заходить на сайт черз одну форму авторизации
    // если по-другому, то этот метод переезжает в наследников
    public void enterSiteUserToSystem(HttpServletRequest req, HttpServletResponse resp, String email, String password, boolean rememberMe) {
        byte[] data1 = password.getBytes(StandardCharsets.UTF_8);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] digest = messageDigest.digest(data1);
        String di = new String(digest);

        Optional<SiteUser> siteUserFromBackendOptional = MyContext.siteUserCRUDRepo.findSiteUserOptByEmailAndPassword(email, di);

        log.debug("siteUserFromBackendOptional: " + siteUserFromBackendOptional);
        log.debug("email: " + email);
        log.debug("password: " + password);

//        int TIME_COOKIE_WITH_TOKEN_LIFE_WITHOUT_REMEMBER_ME = 2000;
//        int TIME_COOKIE_WITH_TOKEN_LIFE_WITH_REMEMBER_ME = 2000*2000;

        if (siteUserFromBackendOptional.isPresent()) {
            log.debug("siteUserCustomerFromBackend isPresent");

            SiteUser siteUserFromBackend = siteUserFromBackendOptional.get();
            log.debug("token that was in backend" + siteUserFromBackend.getToken());

            Cookie cookieWithToken = new Cookie("token", siteUserFromBackend.getToken().toString());
            cookieWithToken.setHttpOnly(true);
            if (rememberMe) {
                cookieWithToken.setMaxAge(TIME_COOKIE_WITH_TOKEN_LIFE_WITH_REMEMBER_ME);//todo 2 days
            } else {
                cookieWithToken.setMaxAge(TIME_COOKIE_WITH_TOKEN_LIFE_WITHOUT_REMEMBER_ME);//todo 2 days
            }
            resp.addCookie(cookieWithToken);

            /*cookie.setMaxAge(2000);//todo 2 days
            //todo token пользователя должен ссылаться на token в системе*/
            //req.getCookies()[0].setMaxAge(0);
            /*resp.addCookie(cookieWithToken);*/

//            MyContext.simpleSiteUserRepository.updateVotedToTrue(siteUser.getToken());
//            siteUserFromBackend.setLogined(true);
            /*log.debug("siteUser " + siteUserFromBackend);
            boolean siteUserInBackendUpdated = MyContext.siteUserCRUDRepo.update(siteUserFromBackend);
            log.debug("siteUserInBackendUpdated " + siteUserInBackendUpdated);*/

            try {
                resp.sendError(HttpServletResponse.SC_OK);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            /*try {
                log.info("redirect to home");
                resp.sendRedirect("http://localhost:8080/home");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }*/

                /*try {
                    log.info("redirect to reg");
                    resp.sendRedirect("http://localhost:8080/reg");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }*/


            //Cookie rememberMeCookie = new Cookie("rememberMe", "true");


            //check
            /*Optional<SiteUser> siteUserOptAfterUpdating = MyContext.siteUserCRUDRepo.findSiteUserOptByEmailAndPassword(email, password);
            if (siteUserOptAfterUpdating.isPresent()) {
                SiteUser siteUserAfterUpdating = siteUserOptAfterUpdating.get();
                if (!siteUserAfterUpdating.isLogined()) {
                    throw new RuntimeException("logined to system, but siteUser in db isnt logined");
                }
            }*/

//                    resp.sendRedirect("http://localhost:8080/task2/home");
//                    RequestDispatcher dispatcher = getServletContext()
//                            .getRequestDispatcher("/task2/home");
//                    dispatcher.forward(req, resp);

        } else {
            /*req.setAttribute("wrongLoginAndPassword", "true");
            log.debug("siteUser is not present");
            try {
                log.info("redirect to login");
                resp.sendRedirect("http://localhost:8080/login?error=true");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }*/

            // no user with such email and password
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            try {
                resp.setContentType("json/text");
                resp.getWriter().println("{" + "\"error:\"" + "incorrect email and password" + "}");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }
    }

    protected abstract Logger getLogger();


}
