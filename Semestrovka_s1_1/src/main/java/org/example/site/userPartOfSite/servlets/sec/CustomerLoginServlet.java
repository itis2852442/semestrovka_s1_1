package org.example.site.userPartOfSite.servlets.sec;

//import org.example.model.Credentials;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.freemarker.FreemarkerUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@WebServlet(name = "CustomerLoginServlet", urlPatterns = "/login")
public class CustomerLoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        /*Cookie cookie = null;
        String reqToken = null;


        Optional<Cookie> cookieOpt = Helper.findCookieWithName(req.getCookies(), "token");
        if (cookieOpt.isPresent()) {
            cookie = cookieOpt.get();
//set expires
            resp.addCookie(cookie);

            reqToken = cookie.getValue();
        }*/

        //fixme unset cookie
        Template template = FreemarkerTemplates.getCustomerLogin();
        HashMap<String, Object> map = new HashMap<>();
            resp.setContentType("text/html");
            //
//            List<SiteUser> siteUserList = MyContext.siteUserRepository.getAll();
//            map.put("siteUsers",siteUserList);

            try {
                template.process(map, resp.getWriter());
            } catch (TemplateException e) {
                throw new RuntimeException(e);
            }

//        if (h.isDBHasToken(reqToken)) {
//            if (h.getTokenAndCredentialsMap().find(reqToken).isLogined()) {
//                resp.sendRedirect("http://localhost:8080/task2/home");
//            }
//            else{
//                resp.getWriter().println(s);
//            }
//        }
//        else{
//            resp.getWriter().println(s);
//            //resp.sendRedirect("http://localhost:8080/task2/reg");
//        }

        //resp.getWriter().println(s);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        //todo "post" auth everywhere
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        boolean rememberMe = Boolean.parseBoolean(req.getParameter("rememberMe"));

        //UUID uuid = MyContext.siteUserSecurityService.enterSiteUserToSystem(email, password, rememberMe);

        MyContext.siteCustomerSecurityService.enterSiteUserToSystem(req, resp, email, password, rememberMe);

    }
}
