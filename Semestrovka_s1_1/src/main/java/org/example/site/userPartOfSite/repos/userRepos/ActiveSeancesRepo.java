/*
package org.example.repos.userRepos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.models.ActiveSeance;
import org.example.repos.CRUDRepository;
import org.example.repos.JDBCRepository;
import org.example.repos.userRepos.absClasses.SiteUserRepositoryOnJDBC;
import org.example.utils.mappers.ActiveSeanceRowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ActiveSeancesRepo extends JDBCRepository<ActiveSeance> implements CRUDRepository<ActiveSeance> {

    //language=sql
    private final String SELECT_ALL_ACTIVE_SEANCES = "select * from active_seances";

    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD_EQ =
            "select * from active_seances " +
                    "where $field = ?";

    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ =
            "select * from active_seances " +
                    "where $field1 = ? and $field2 = ?";

    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ_FIELD3_EQ =
            "select * from active_seances " +
                    "where $field1 = ? and $field2 = ? and $field3 = ?";

    //language=sql
    protected final String SQL_DELETE_WHERE_FIELD_EQ =
            "select * from active_seances " +
                    "where $field = ?";

    protected final String LIMIT_1 = "Limit 1";

    private static final Logger log = LogManager.getLogger(ActiveSeancesRepo.class);

    public ActiveSeancesRepo(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    public boolean add(ActiveSeance activeSeance) {
        return false;
    }

    @Override
    public Optional<ActiveSeance> find(String id) {
        return Optional.empty();
    }

    @Override
    public List<ActiveSeance> getAll() {

    }

    protected List<ActiveSeance> getListActiveSeancesWhereFieldEq(String fieldName, String value) {
//        if (value == null) {
//            log.warn(fieldName + " is null");
//            return new ArrayList<>();
//        }
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName));
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<ActiveSeance> listOfActiveSeances = getListFromResultSet(resultSet, new ActiveSeanceRowMapper());

            return listOfActiveSeances;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<ActiveSeance> findFirstSiteUserWhereField1EqField2Eq(String field1Name, String value1,String field2Name, String value2) {
//        if (value == null) {
//            log.warn(fieldName + " is null");
//            return Optional.empty();
//        }
        try (Connection connection = getConnection()) {
            log.debug("fieldName " + fieldName + "value " + value);
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName) +
                                    " " + LIMIT_1);
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<ActiveSeance> listOfActiveSeances = getListFromResultSet(resultSet, new ActiveSeanceRowMapper());

            return listOfActiveSeances.stream()
                    .findFirst();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<ActiveSeance> getListActiveSeancesWhereField1EqField2Eq(String field1Name, String value1,String field2Name, String value2) {
//        if (value == null) {
//            log.warn(fieldName + " is null");
//            return new ArrayList<>();
//        }
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName));
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<ActiveSeance> listOfActiveSeances = getListFromResultSet(resultSet, new ActiveSeanceRowMapper());

            return listOfActiveSeances;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<ActiveSeance> findFirstSiteUserWhereFieldEq(String fieldName, String value) {
//        if (value == null) {
//            log.warn(fieldName + " is null");
//            return Optional.empty();
//        }
        try (Connection connection = getConnection()) {
            log.debug("fieldName " + fieldName + "value " + value);
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName) +
                                    " " + LIMIT_1);
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<ActiveSeance> listOfActiveSeances = getListFromResultSet(resultSet, new ActiveSeanceRowMapper());

            return listOfActiveSeances.stream()
                    .findFirst();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean update(ActiveSeance activeSeance) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
*/
