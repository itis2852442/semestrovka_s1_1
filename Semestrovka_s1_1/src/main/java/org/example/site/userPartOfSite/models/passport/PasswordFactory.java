package org.example.site.userPartOfSite.models.passport;

import org.example.site.userPartOfSite.models.passport.Impl.ChinaPassport;
import org.example.site.userPartOfSite.models.passport.Impl.JapanPassport;
import org.example.site.userPartOfSite.models.passport.Impl.RussianPassport;
import org.example.site.userPartOfSite.models.passport.Impl.USAPassport;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;
import org.example.site.utils.enums.Country;

public class PasswordFactory {

    public static Passport create(Country country, String digits) {
        switch (country) {
            case RUSSIA -> {
                if (digits.length() != 10) {
                    return null;
                } else {
                    return RussianPassport.builder()
                            .nomer(digits.substring(0, 5))
                            .seria(digits.substring(5, 10))
                            .build();
                }
            }
            case USA -> {
                if (digits.length() != 15) {
                    return null;
                } else {//fixme all passports
                    return USAPassport.builder()
                            .nomer("12")
                            .seria("12")
                            .build();
                }
            }
            case JAPAN -> {
                if (digits.length() != 21) {
                    return null;
                } else {
                    return JapanPassport.builder()
                            .nomer("12")
                            .build();
                }
            }
            case CHINA -> {
                if (digits.length() != 30) {
                    return null;
                } else {
                    return ChinaPassport.builder()
                            .nomer("12")
                            .seria("12")
                            .build();
                }
            }
            default -> {
                return null;
            }
        }
    }

}
