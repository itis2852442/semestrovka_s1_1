package org.example.site.userPartOfSite.services.users.sec;

import org.example.site.utils.helpers.Helper;
import org.example.site.utils.locations.Locations;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.Optional;


@WebServlet(name = "LogOutServlet", urlPatterns = "/logOut")
public class LogOutServlet extends HttpServlet {
//todo delete cookie after log outing

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Optional<Cookie> cookieOpt = Helper.findCookieWithName(req.getCookies(), "token");
        if (cookieOpt.isPresent()) {
            Cookie cookie = cookieOpt.get();

            //setting max age = 0 to cookie
            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }

//        resp.sendError(HttpServletResponse.SC_OK);
//        resp.setContentType("json/text");
//        resp.getWriter().println();

        resp.sendRedirect(req.getContextPath() + Locations.getCustomerHome());
    }
}
