package org.example.site.userPartOfSite.filters;

import lombok.extern.log4j.Log4j2;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.helpers.Helper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;



@WebFilter(filterName = "OnlyCustomerAndUnAuthorizedAccessFilter")
@Log4j2
public class OnlyCustomerAndUnAuthorizedAccessFilter extends HttpFilter {

    @Override
    public void destroy() {

    }

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {

        Cookie cookie = null;
        String reqToken = null;

        Optional<Cookie> cookieOpt = Helper.findCookieWithName(req.getCookies(), "token");
        if (cookieOpt.isPresent()) {
            cookie = cookieOpt.get();
            reqToken = cookie.getValue();
        }

        Optional<SiteUser> siteUserOpt = MyContext.siteUserCRUDRepo.find(reqToken);
        if (siteUserOpt.isPresent()) {
            log.debug("siteUser is present");
            SiteUser siteUser = siteUserOpt.get();
            if (SiteUserRole.SIMPLE_CUSTOMER.equals(siteUser.getRole())) {
                log.debug("siteUser is customer");
                log.info("entering user");
                chain.doFilter(req, resp);
            } else {
                log.debug("siteUser isnt customer");
                log.info("sendError SC_FORBIDDEN");
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else {
            log.debug("siteUser is unAuthorized");
            log.info("entering user");
            chain.doFilter(req, resp);
        }

    }
}
