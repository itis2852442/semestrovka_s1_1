package org.example.site.userPartOfSite.repos;

import org.example.site.userPartOfSite.utils.mappers.RowMapper;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class JDBCRepository<T> {

    private final String url;
    private final String user;
    private final String password;

    //protected Logger log = getLogger();

    //public abstract Logger getLogger();

    public JDBCRepository(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection() {
        try {
            Class.forName("org.postgresql.Driver").getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException |
                 NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> getListFromResultSet(ResultSet resultSet, RowMapper<T> rowMapper) throws SQLException {
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(rowMapper.from(resultSet));
        }
        return list;
    }




}
