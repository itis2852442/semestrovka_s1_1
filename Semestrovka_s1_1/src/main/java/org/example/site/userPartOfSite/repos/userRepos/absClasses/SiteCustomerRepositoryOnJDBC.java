package org.example.site.userPartOfSite.repos.userRepos.absClasses;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteCustomerRepository;
import org.example.site.userPartOfSite.utils.mappers.SiteCustomerRowMapper;

public abstract class SiteCustomerRepositoryOnJDBC extends SiteUserRepositoryOnJDBC<SiteCustomer> implements SiteCustomerRepository {

    public SiteCustomerRepositoryOnJDBC(String url, String user, String password) {
        super(url, user, password);
        super.rowMapper = new SiteCustomerRowMapper();
    }

    @Override
    protected String getTABLE_NAME() {
        return "site_customer";
    }
}
