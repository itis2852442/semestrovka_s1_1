package org.example.site.userPartOfSite.models.passport.Impl;

import lombok.*;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;
import org.example.site.utils.enums.Country;

@Getter
@Setter
public class USAPassport extends Passport {

    @NonNull
    private String nomer;
    @NonNull
    private String seria;

    @Builder
    public USAPassport(String nomer, String seria) {
        super(Country.USA);
        this.nomer=nomer;
        this.seria=seria;
    }

    @Override
    public String getDigits() {
        return nomer + seria;
    }
}
