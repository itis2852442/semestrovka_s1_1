package org.example.site.userPartOfSite.servlets.customer;

import org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer.BuyTicketDTO;
import org.example.site.utils.context.MyContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(name = "BuyingTicketServlet", urlPatterns = "/customer/buyingTicket")
public class BuyingTicketServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BuyTicketDTO buyTicketDTO = BuyTicketDTO.builder()
                .flightUUID((UUID) req.getAttribute("flightUUID"))
                .place(req.getIntHeader("place"))
                .build();
        resp.setContentType("json/text");
        resp.getWriter().println(MyContext.ticketsService.buyTicket(buyTicketDTO));

    }
}
