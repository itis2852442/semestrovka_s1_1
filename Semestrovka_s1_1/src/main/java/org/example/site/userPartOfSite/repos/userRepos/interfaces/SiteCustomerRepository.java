package org.example.site.userPartOfSite.repos.userRepos.interfaces;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;

public interface SiteCustomerRepository extends SiteUserRepository<SiteCustomer> {
}
