package org.example.site.userPartOfSite.services.users.sec;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.utils.context.MyContext;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public class SiteWorkerSecurityService extends SiteUserSecurityService{

    public SiteWorkerSecurityService() {
        super.TIME_COOKIE_WITH_TOKEN_LIFE_WITHOUT_REMEMBER_ME = 2000;
        super.TIME_COOKIE_WITH_TOKEN_LIFE_WITH_REMEMBER_ME = 2000 * 2000;
    }


    public UUID registerSiteWorkerWithAllChecks(SiteWorker siteWorker) {
        /*String reqToken = null;

        Optional<Cookie> cookieWithTokenOpt = Helper.findCookieWithName(req.getCookies(), "token");
        if (cookieWithTokenOpt.isPresent()) {
            reqToken = cookieWithTokenOpt.get().getValue();
        }*/

//        if (!h.isDBHasToken(reqToken)) {
////            System.out.println("reqToken reg 2 "+reqToken);
//            if(reqToken==null){
////                System.out.println("reqToken reg 3 "+reqToken);
//                String generatedToken = UUID.randomUUID().toString();
//                Cookie cookie = new Cookie("token",generatedToken);
//                resp.addCookie(cookie);
//                h.getTokenAndCredentialsMap().put(generatedToken, new Credentials(login,password));
////                System.out.println(h.getTokenAndCredentialsMap()+"r");
//            }
//            else {
////                System.out.println("reqToken reg 4 "+reqToken);
//                h.getTokenAndCredentialsMap().put(reqToken, new Credentials(login,password));
////                System.out.println(h.getTokenAndCredentialsMap()+"f");
//            }
//            String s = """
//                    <html>
//                        <body>
//                            <h1>you have been registered</h1>
//                            <form action="http://localhost:8080/task2/login">
//                                <input type="submit" value="to login page">
//                            </form>
//                        </body>
//                    </html>
//                    """;
//            resp.getWriter().println(s);
//
////            RequestDispatcher dispatcher = getServletContext()
////                    .getRequestDispatcher("http://localhost:8080/task2/login");
////            dispatcher.forward(req, resp);
//        }else{
////            System.out.println("reqToken reg 5 "+reqToken);
//            resp.sendRedirect("http://localhost:8080/task2/login");
//        }
        Optional<SiteCustomer> siteCustomer1 = MyContext.siteUserCustomerCRUDRepository.findSiteUserOptByEmail(siteWorker.getEmail());
        Optional<SiteCustomer> siteCustomer2 = MyContext.siteUserCustomerCRUDRepository.findSiteUserOptByPhone(siteWorker.getPhoneNumber());
        if(siteCustomer1.isPresent() || siteCustomer2.isPresent()){
            return null;
        }

        byte[] data1 = siteWorker.getPassword().getBytes(StandardCharsets.UTF_8);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] digest = messageDigest.digest(data1);
        String di = new String(digest);

        siteWorker.setPassword(di);
        UUID genUUID =  UUID.fromString(MyContext.siteWorkerCRUDRepository.add(siteWorker));

        log.debug("generated UUID: " + genUUID);
        return genUUID;
        /*String generatedToken = UUID.randomUUID().toString();
        Cookie generatedCookie = new Cookie("token", generatedToken);
        generatedCookie.setMaxAge(2000 * 2000);//todo 2 days
        resp.addCookie(generatedCookie);
        //fixme remove this constructor in siteUserCustomer
        boolean addedNewSiteCustomerToBackend = MyContext.siteUserCustomerCRUDRepository.add(new SiteCustomer(
                generatedToken, email, password, false, SiteUserRole.SIMPLE_CUSTOMER));

        if (addedNewSiteCustomerToBackend) {
            try {
                resp.sendRedirect("http://localhost:8080/login");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                //fixme error=true
                resp.sendRedirect("http://localhost:8080/reg?error=true");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/

        //check; todo

    }


    @Override
    protected Logger getLogger() {
        return log;
    }
}
