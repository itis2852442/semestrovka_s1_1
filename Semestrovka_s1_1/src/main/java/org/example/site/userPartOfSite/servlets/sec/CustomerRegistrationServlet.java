package org.example.site.userPartOfSite.servlets.sec;

//import org.example.model.Credentials;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.Country;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.exceptions.ServiceException;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.freemarker.FreemarkerUtil;
import org.example.site.utils.locations.Locations;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

@WebServlet(name = "CustomerRegistrationServlet", urlPatterns ="/reg")
@Log4j2
public class CustomerRegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        Cookie cookie = null;
//        String reqToken = null;
//        Optional<Cookie> cookieOpt = Helper.findCookieWithName(req.getCookies(), "token");
//        if (cookieOpt.isPresent()) {
//            cookie = cookieOpt.get();
//
//            resp.addCookie(cookie);
//
//            reqToken = cookie.getValue();
//        }
        log.debug("getLocalAddr: " + req.getLocalAddr());
        log.debug("getLocalPort: " + req.getLocalPort());
        log.debug("getRemotePort: " + req.getRemotePort());
        log.debug("getServerPort: " + req.getServerPort());

//        if (h.isDBHasToken(reqToken)) {
//            if (h.getTokenAndCredentialsMap().find(reqToken).isLogined()) {
//                resp.sendRedirect("http://localhost:8080/task2/home");
//            }
//            else{
//                resp.sendRedirect("http://localhost:8080/task2/login");
//            }
//        }
//        else{
//            String s = """
//                        <html>
//                            <body>
//                                <h1><i>registration</i></h1>
//                                <form action="http://localhost:8080/task2/reg" method='POST'>
//                                    <input type="text" name="login" required>
//                                    <input type="password" name="password" required>
//                                    <input type="submit" value="register">
//                                </form>
//                                <hr>
//                                <form action="http://localhost:8080/task2/login">
//                                    <input type="submit" value="login page">
//                                </form>
//                            </body>
//                        </html>
//                        """;
//            resp.getWriter().println(s);
//        }
        Template template = FreemarkerTemplates.getCustomerReg();
        HashMap<String, Object> map = new HashMap<>();

//        List<SiteUser> siteUserList = MyContext.siteUserRepository.getAll();
//        map.put("siteUsers",siteUserList);

        resp.setContentType("text/html");
        try {
            template.process(map, resp.getWriter());
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
//        resp.getWriter().println(s);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        System.out.println("reqToken reg "+reqToken);

        //log.debug("!!!!: " + req.getParameter("firstName"));

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String patronymic = req.getParameter("patronymic");
        String age = req.getParameter("age");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passportCountry = req.getParameter("passportCountry");
        if(passportCountry!=null){
            passportCountry = passportCountry.toUpperCase();
        }
        String passportDigits = req.getParameter("passportDigits");


        MyContext.siteCustomerSecurityService.registerSiteCustomer(req,resp,
                firstName,
                lastName,
                patronymic,
                age,
                phoneNumber,
                email,
                password,
                passportCountry,
                passportDigits
                );



        /*//
        Optional<SiteUser> siteUserOpt = SiteUserHelper.findSiteUserOptByReq(req);
        if(siteUserOpt.isPresent()){
            SiteUser siteUser = siteUserOpt.get();
            if(siteUser instanceof SiteCustomer){
                //String passportDigits = req.getParameter("passportDigits");
                //todo other credentials
                //fixme
            }
            else if(siteUser instanceof SiteWorker){
                MyContext.siteCustomerService.registerSiteCustomer(req, resp, login, password);
            }
        }
        //*/

//        System.out.println(h.getTokenAndCredentialsMap());


        //fixme redirect
        /*Template template = FreemarkerUtil.getConfig().getTemplate("reg/successReg.ftl");
        HashMap<String, Object> map = new HashMap<>();*/

//        List<SiteUser> siteUserList = MyContext.siteUserRepository.getAll();
//        map.put("siteUsers",siteUserList);

        //fixme change to json
        /*resp.setContentType("text/html");
        try {
            template.process(map, resp.getWriter());
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }*/

//        RequestDispatcher dispatcher = getServletContext()
//                .getRequestDispatcher("http://localhost:8080/task2/login");
//        dispatcher.forward(req, resp);
    }
}
