package org.example.site.userPartOfSite.models.passport.Impl;

import lombok.*;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;
import org.example.site.utils.enums.Country;

@Getter
@Setter
public class JapanPassport extends Passport {

    @NonNull
    private String nomer; //12 digits

    @Builder
    public JapanPassport(String nomer) {
        super(Country.CHINA);
        this.nomer=nomer;
    }

    @Override
    public String getDigits() {
        return nomer;
    }
}
