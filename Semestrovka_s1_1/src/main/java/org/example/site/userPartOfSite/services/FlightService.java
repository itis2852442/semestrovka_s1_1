package org.example.site.userPartOfSite.services;

import org.example.site.infrastructurePartOfSite.models.Flight;
import org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer.SearchAvailableFlightsFromCityToCityWhenDTO;
import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.utils.context.MyContext;

import java.util.List;
import java.util.UUID;

public class FlightService {

    public List<Flight> findAvaliableFlights(SearchAvailableFlightsFromCityToCityWhenDTO searchAvailableFlightsFromCityToCityWhenDTO){
        return MyContext.FlightInfrService.findAvaliableFlights(searchAvailableFlightsFromCityToCityWhenDTO);
    }

    public List<Ticket> finfAvailiableTicketsOnFlight(UUID flightUUID){
        return MyContext.FlightInfrService.findAvailiableTicketsForFlight(flightUUID);
    }

}
