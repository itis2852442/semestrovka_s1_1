package org.example.site.userPartOfSite.models.flights;

import lombok.*;
import org.example.site.infrastructurePartOfSite.models.PlaceInFlight;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;

import java.sql.Time;
import java.util.Date;

@Getter
@Setter
public class Ticket {

    @NonNull
    private SiteCustomer siteCustomer;
    @NonNull
    private PlaceInFlight placeInFlight;
    @NonNull
    private Integer cost;
    @NonNull
    private Date dateOfBought;
    @NonNull
    private Time timeOfBought;

    @Builder
    public Ticket(SiteCustomer siteCustomer,
                  PlaceInFlight placeInFlight,
                  Integer cost,
                  Date dateOfBought,
                  Time timeOfBought) {
        this.siteCustomer = siteCustomer;
        this.placeInFlight = placeInFlight;
        this.cost = cost;
        this.dateOfBought = dateOfBought;
        this.timeOfBought = timeOfBought;
    }

}
