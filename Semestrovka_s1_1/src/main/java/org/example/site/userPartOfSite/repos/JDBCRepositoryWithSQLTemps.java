package org.example.site.userPartOfSite.repos;

import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.helpers.Helper;

import java.sql.*;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public abstract class JDBCRepositoryWithSQLTemps<T> extends JDBCRepository<T> {

    protected Logger log = getLogger();

    protected RowMapper<T> rowMapper = getRowMapper();

    //language=sql
    protected String SQL_INSERT = "insert into "+ getTABLE() + getFIELDS()+" values "+getFIELDSQ();

    //language=sql
    protected String SQL_SELECT_ALL = "select * from "+ getTABLE();

    //language=sql
    protected String SQL_SELECT_WHERE_FIELD_EQ = "select * from "+ getTABLE() +" where %1s = ?";

    //language=sql
    protected String SQL_SELECT_WHERE_FIELD1_EQ_Field2_EQ = "select * from "+ getTABLE() +" where %1s = ? %2s = ?";

    //language=sql
    protected String SQL_DELETE_BY_PK = "delete from "+ getTABLE() +" where "+getPK() + " = ?";
    //language=sql
    private String SQL_SELECT_WHERE_FIELD1_EQ_Field2_EQ_Field3_EQ = "select * from "+ getTABLE() +" where %1s = ? %2s = ? %3s = ?";


    public JDBCRepositoryWithSQLTemps(String url, String user, String password) {
        super(url, user, password);
    }


    protected abstract Logger getLogger();

    protected abstract RowMapper<T> getRowMapper();
    protected abstract String getTABLE();
    protected abstract String getPK();
    //(country, city, age)
    protected abstract String getFIELDS();
    //(? ? ?)
    protected abstract String getFIELDSQ();

    protected abstract void putToPrepStat(PreparedStatement statement, T t);


    public String add(T t) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            putToPrepStat(statement, t);
            log.trace("sql: " + statement);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 1) {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    log.debug("TRYING TO GET UUID FROM DB: ");
                    if (generatedKeys.next()) {
                        return generatedKeys.getString(1);
                    }
                    else {
                        throw new SQLException("failed: no UUID");
                    }
                }
            } else if (affectedRows == 0) {
                return null;
            } else {
                throw new RuntimeException("collisiums");
            }

        } catch (SQLException e) {
            Helper.printSQLException(e);
            if(e.getErrorCode()==0){
                return null;
            }
            else{
                throw new RuntimeException(e);
            }
        }
    }


    public Optional<T> find(String id) {
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(String.format(SQL_SELECT_WHERE_FIELD_EQ, getPK()));

            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = getListFromResultSet(resultSet,rowMapper);

            return res.stream().findFirst();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<T> getAll() {
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL);

            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = getListFromResultSet(resultSet,rowMapper);

            return res;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> getListWhereFieldEq(String fieldName, String value) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            String.format(SQL_SELECT_WHERE_FIELD_EQ, fieldName));
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<T> list = getListFromResultSet(resultSet, rowMapper);

            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> getListWhereField1EqField2Eq(String field1Name, String value1, String field2Name, String value2) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            String.format(SQL_SELECT_WHERE_FIELD1_EQ_Field2_EQ, field1Name, field2Name));
            statement.setString(1, value1);
            statement.setString(2, value2);
            log.trace("sql: " + statement);
            ResultSet resultSet = statement.executeQuery();

            List<T> list = getListFromResultSet(resultSet, rowMapper);

            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<T> getListWhereField1EqField2EqField3Eq(String field1Name, String value1, String field2Name, String value2, String field3Name, String value3) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            String.format(SQL_SELECT_WHERE_FIELD1_EQ_Field2_EQ_Field3_EQ, field1Name, field2Name, field3Name));
            statement.setString(1, value1);
            statement.setString(2, value2);
            statement.setString(3, value3);
            log.trace("sql: " + statement);
            ResultSet resultSet = statement.executeQuery();

            List<T> list = getListFromResultSet(resultSet, rowMapper);

            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
