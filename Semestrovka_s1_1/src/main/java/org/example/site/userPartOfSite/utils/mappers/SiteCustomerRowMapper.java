package org.example.site.userPartOfSite.utils.mappers;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.enums.Country;
//import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.UUID;


public class SiteCustomerRowMapper extends AbstractSiteUserRowMapper<SiteCustomer> {
    public SiteCustomer from(ResultSet rs) throws SQLException {
        //fixme
        SiteCustomer res = SiteCustomer.builder()
                .token(UUID.fromString(rs.getString(1)))
                .firstName(rs.getString(2))
                .lastName(rs.getString(3))
                .patronymic(rs.getString(4))
                .age(rs.getInt(5))
                .password(rs.getString(6))
                .banned(rs.getBoolean(7))
                .role(SiteUserRole.valueOf(rs.getString(8)))
                .passport(Objects.requireNonNull(PasswordFactory.create(Country.valueOf(rs.getString(9)), rs.getString(10))))
                .phoneNumber(rs.getString(11))
                .email(rs.getString(12))
                .build();
        /*SiteCustomer res = new SiteCustomer(

                rs.getString(2),
                rs.getString(3),
                rs.getDate(4),
                rs.getString(5),
                rs.getString(6),
                rs.getString(7),
                rs.getString(8),
                SiteUserRole.valueOf(rs.getString(9)),
                rs.getBoolean(10)
        );*/
        return res;


    }
}


    /*SiteCustomer res = SiteCustomer.builder()
            .token(rs.getString(1))
            .firstName()
            .lastName()
            .patronymic()
            .passport()
            .build();
                ,
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        SiteUserRole.valueOf(rs.getString(9)),
                        rs.getBoolean(10),
                        rs.getBoolean(11));*/