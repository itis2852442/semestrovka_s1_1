package org.example.site.userPartOfSite.models.media;


import lombok.*;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;

@Getter
@Setter
public class Comment {

    private SiteCustomer siteCustomer;
    private Headline headline;
    private String content;

}
