package org.example.site.userPartOfSite.models.humans.siteUsers.Impl;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
//import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class SiteWorker extends SiteUser {

    public SiteWorker() {
    }

    @Builder
    public SiteWorker(@NonNull String firstName,
                      @NonNull String lastName,
                      @NonNull String patronymic,
                      int age,
                      @NonNull String phoneNumber,
                      UUID token,
                      @NonNull String email,
                      @NonNull String password,
                      @NonNull SiteUserRole role,
                      boolean banned) {
        super(firstName, lastName, patronymic, age, phoneNumber, token, email, password, role, banned);
    }
}
