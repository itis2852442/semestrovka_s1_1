package org.example.site.userPartOfSite.services;

import org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer.BuyTicketDTO;
import org.example.site.utils.todelete.dto2.response.BuyTicketResponse;
import org.example.site.utils.context.MyContext;

public class TicketsService {

    public BuyTicketResponse buyTicket(BuyTicketDTO buyTicketDTO){
        return MyContext.ticketInfrService.buyTicketWithChecking(buyTicketDTO);
    }

}
