package org.example.site.userPartOfSite.repos.mediaRepo;

import org.example.site.userPartOfSite.models.media.Headline;
import org.example.site.userPartOfSite.repos.mediaRepo.interfaces.HeadLineRepo;
import org.example.site.userPartOfSite.repos.JDBCRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class HeadLineRepoJDBCImpl extends JDBCRepository<Headline> implements HeadLineRepo {

    public HeadLineRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    public String add(Headline headline) {
        return null;
    }

    @Override
    public Optional<Headline> find(String id) {
        return Optional.empty();
    }

    @Override
    public List<Headline> getAll() {
        return null;
    }

    @Override
    public boolean update(Headline headline) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
