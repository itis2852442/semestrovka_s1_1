package org.example.site.userPartOfSite.services.users.sec;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.Country;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public class SiteCustomerSecurityService extends SiteUserSecurityService {

    public SiteCustomerSecurityService() {
        super.TIME_COOKIE_WITH_TOKEN_LIFE_WITHOUT_REMEMBER_ME = 20_000;
        super.TIME_COOKIE_WITH_TOKEN_LIFE_WITH_REMEMBER_ME = 2000 * 2000;
    }


    private UUID registerSiteCustomer(SiteCustomer siteCustomer) {
        /*String reqToken = null;

        Optional<Cookie> cookieWithTokenOpt = Helper.findCookieWithName(req.getCookies(), "token");
        if (cookieWithTokenOpt.isPresent()) {
            reqToken = cookieWithTokenOpt.get().getValue();
        }*/

        /*if (!h.isDBHasToken(reqToken)) {
//            System.out.println("reqToken reg 2 "+reqToken);
            if(reqToken==null){
//                System.out.println("reqToken reg 3 "+reqToken);
                String generatedToken = UUID.randomUUID().toString();
                Cookie cookie = new Cookie("token",generatedToken);
                resp.addCookie(cookie);
                h.getTokenAndCredentialsMap().put(generatedToken, new Credentials(login,password));
//                System.out.println(h.getTokenAndCredentialsMap()+"r");
            }
            else {
//                System.out.println("reqToken reg 4 "+reqToken);
                h.getTokenAndCredentialsMap().put(reqToken, new Credentials(login,password));
//                System.out.println(h.getTokenAndCredentialsMap()+"f");
            }
            String s = """
                    <html>
                        <body>
                            <h1>you have been registered</h1>
                            <form action="http://localhost:8080/task2/login">
                                <input type="submit" value="to login page">
                            </form>
                        </body>
                    </html>
                    """;
            resp.getWriter().println(s);

//            RequestDispatcher dispatcher = getServletContext()
//                    .getRequestDispatcher("http://localhost:8080/task2/login");
//            dispatcher.forward(req, resp);
        }else{
//            System.out.println("reqToken reg 5 "+reqToken);
            resp.sendRedirect("http://localhost:8080/task2/login");
        }*/

        /*String generatedToken = UUID.randomUUID().toString();
        Cookie generatedCookie = new Cookie("token", generatedToken);
        generatedCookie.setMaxAge(2000 * 2000);//todo 2 days
        resp.addCookie(generatedCookie);*/

        //fixme remove this constructor in siteUserCustomer

        //todo in separated table
        Optional<SiteWorker> siteWorker1 = MyContext.siteWorkerCRUDRepository.findSiteUserOptByEmail(siteCustomer.getEmail());
        Optional<SiteWorker> siteWorker2 = MyContext.siteWorkerCRUDRepository.findSiteUserOptByPhone(siteCustomer.getPhoneNumber());
        if(siteWorker1.isPresent() || siteWorker2.isPresent()){
            return null;
        }

        byte[] data1 = siteCustomer.getPassword().getBytes(StandardCharsets.UTF_8);
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] digest = messageDigest.digest(data1);
        String di = new String(digest);

        siteCustomer.setPassword(di);

        String genUUIDStr = MyContext.siteUserCustomerCRUDRepository.add(siteCustomer);

        UUID genUUID = genUUIDStr==null? null :UUID.fromString(genUUIDStr);
        log.debug("genUUID: " + genUUID);
        return genUUID;


        /*if (addedNewSiteCustomerToBackend) {
            try {
                resp.sendRedirect("http://localhost:8080/login");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                //fixme error=true
                resp.sendRedirect("http://localhost:8080/reg?error=true");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/

        //check; todo

    }

    public void registerSiteCustomer(HttpServletRequest req, HttpServletResponse resp,
                                     String firstName,
                                     String lastName,
                                     String patronymic,
                                     String age,
                                     String phoneNumber,
                                     String email,
                                     String password,
                                     String passportCountry,
                                     String passportDigits){

        boolean correct = false;
        try {
            correct = MyContext.checkerService.checkSiteCustomerReg(
                    firstName,
                    lastName,
                    patronymic,
                    age,
                    phoneNumber,
                    email,
                    password,
                    passportCountry,
                    passportDigits);
        } catch (Exception e) {
            if(e instanceof ServiceException){
                log.debug("send error 400 to client: ");
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                log.debug("set status 400");
                try {
                    resp.getWriter().println("{\"error: \""+ e.getMessage() +"}");
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                //resp.sendError(HttpServletResponse.SC_CONFLICT, e.getMessage());
            }
            else{
                throw new RuntimeException();
            }
        }



        if(correct){
            log.debug("checkSiteCustomerReg: " + correct);
            SiteCustomer siteCustomerToReg = SiteCustomer.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .patronymic(patronymic)
                    .age(Integer.parseInt(age))
                    .phoneNumber(phoneNumber)
                    .email(email)

                    .password(password)
                    .passport(Objects.requireNonNull(PasswordFactory.create(Country.valueOf(passportCountry), passportDigits)))

                    .role(SiteUserRole.SIMPLE_CUSTOMER)
                    .banned(false)
                    .build();

            UUID uuidOfRegisteredCustomer = MyContext.siteCustomerSecurityService.registerSiteCustomer(siteCustomerToReg);

            if (uuidOfRegisteredCustomer == null) {
                //there is another user in db
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                try {
                    resp.getWriter().println("{\"error: \""+ "user exists" +"}");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
//            resp.setContentType("json/text");
//            resp.getWriter().println("{}");
//            resp.sendRedirect(MyContext.locations.getProperty("login"));
            } else {
                //registered
                //todo check in postman
                /*Cookie generatedCookie = new Cookie("token", uuidOfRegisteredCustomer.toString());
                generatedCookie.setMaxAge(2000 * 2000);//todo 2 days
                resp.addCookie(generatedCookie);*/

//                t odo forward login with login and password
                resp.setStatus(HttpServletResponse.SC_OK);

                //log.debug("sending client to: " + MyContext.locations.getProperty("login"));
//                resp.sendRedirect(MyContext.locations.getProperty("login"));
//                resp.sendRedirect("http://localhost:8080/login");
            }
        }

    }


    @Override
    protected Logger getLogger() {
        return log;
    }
}
