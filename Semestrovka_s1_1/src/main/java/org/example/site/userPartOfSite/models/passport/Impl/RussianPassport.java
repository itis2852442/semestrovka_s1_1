package org.example.site.userPartOfSite.models.passport.Impl;

import lombok.*;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;
import org.example.site.utils.enums.Country;

@Getter
@Setter
public class RussianPassport extends Passport {

    @NonNull
    private String nomer;
    @NonNull
    private String seria;

    @Builder
    public RussianPassport(String nomer, String seria) {
        super(Country.RUSSIA);
        this.nomer=nomer;
        this.seria=seria;
    }

    @Override
    public String getDigits() {
        return nomer + seria;
    }
}
