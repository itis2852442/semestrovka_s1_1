package org.example.site.userPartOfSite.repos;

import org.example.site.userPartOfSite.models.flights.Ticket;

public class TicketRepo extends JDBCRepository<Ticket> {

    public TicketRepo(String url, String user, String password) {
        super(url, user, password);
    }

}
