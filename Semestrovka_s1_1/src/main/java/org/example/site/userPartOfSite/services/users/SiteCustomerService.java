package org.example.site.userPartOfSite.services.users;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.passport.PasswordFactory;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.exceptions.ServiceException;
import org.example.site.utils.helpers.Helper;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Log4j2
public class SiteCustomerService extends SiteUserService {


    public void buyTicketBySiteCustomer() {

    }

    //public void

    public void createCommentToHeadline(UUID headlineId) {

    }

}
