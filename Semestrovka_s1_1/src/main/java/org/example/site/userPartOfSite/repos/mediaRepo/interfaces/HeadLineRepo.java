package org.example.site.userPartOfSite.repos.mediaRepo.interfaces;

import org.example.site.userPartOfSite.models.media.Headline;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface HeadLineRepo extends CRUDRepository<Headline> {
}
