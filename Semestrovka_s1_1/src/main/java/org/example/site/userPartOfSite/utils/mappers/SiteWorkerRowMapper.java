package org.example.site.userPartOfSite.utils.mappers;

import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
//import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

public class SiteWorkerRowMapper extends AbstractSiteUserRowMapper<SiteWorker> {
    @Override
    public SiteWorker from(ResultSet rs) throws SQLException {
        //fixme
        SiteWorker res = SiteWorker.builder()
                .token(UUID.fromString(rs.getString(1)))
                .firstName(rs.getString(2))
                .lastName(rs.getString(3))
                .patronymic(rs.getString(4))
                .age(rs.getInt(5))
                .password(rs.getString(6))
                .banned(rs.getBoolean(7))
                .role(SiteUserRole.valueOf(rs.getString(8)))
                .phoneNumber(rs.getString(9))
                .email(rs.getString(10))
                .build();
        return res;
    }
}
