package org.example.site.userPartOfSite.models.humans.siteUsers.Impl;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
//import org.example.site.utils.enums.Gender;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.userPartOfSite.models.passport.absClasses.Passport;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class SiteCustomer extends SiteUser {
    @NonNull
    private Passport passport;
//    private List<Ticket> tickets;

    public SiteCustomer() {
    }

    @Builder
    public SiteCustomer(@NonNull String firstName,
                        @NonNull String lastName,
                        @NonNull String patronymic,
                        int age,
                        @NonNull String phoneNumber,
                        UUID token,
                        @NonNull String email,
                        @NonNull String password,
                        @NonNull SiteUserRole role,
                        boolean banned,
                        @NonNull Passport passport) {
        super(firstName, lastName, patronymic, age, phoneNumber, token, email, password, role, banned);
        this.passport = passport;
    }
}
