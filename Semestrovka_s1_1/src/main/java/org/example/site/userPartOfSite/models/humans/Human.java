package org.example.site.userPartOfSite.models.humans;

//fixme fix all dates

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
//import org.example.site.utils.enums.Gender;

import javax.validation.constraints.Positive;
import java.util.Date;

@Getter
@Setter
public abstract class Human {

    //fixme can be stored in passport
    @NonNull
    protected String firstName;
    @NonNull
    protected String lastName;
    @NonNull
    protected String patronymic;
    @Positive
    protected int age;
    @NonNull
    protected String phoneNumber;

    public Human() {
    }


    public Human(@NonNull String firstName, @NonNull String lastName, @NonNull String patronymic, int age, @NonNull String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }




}
