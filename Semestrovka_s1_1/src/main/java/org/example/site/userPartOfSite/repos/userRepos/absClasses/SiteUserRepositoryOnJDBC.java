package org.example.site.userPartOfSite.repos.userRepos.absClasses;

import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteUserRepository;
import org.example.site.userPartOfSite.utils.mappers.AbstractSiteUserRowMapper;
import org.example.site.userPartOfSite.repos.JDBCRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class SiteUserRepositoryOnJDBC<T extends SiteUser> extends JDBCRepository<T> implements SiteUserRepository<T> {

    protected final String TABLE_NAME = getTABLE_NAME();
    protected AbstractSiteUserRowMapper<T> rowMapper;

    private final Logger log = getLogger();


    /*//language=sql
    protected final String SQL_SELECT_ALL_SITE_USER = "select * from " + TABLE_NAME + " " +
            "inner join ticket t " +
            "on " + TABLE_NAME + ".token = t.id_site_customer";

    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD_EQ =
            "select * from " + TABLE_NAME + " " +
                    "left join ticket on " + TABLE_NAME + ".token = ticket.id_site_customer " +
                    "where $field = ?";
    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ =
            "select * from " + TABLE_NAME + " " +
                    "left join ticket on " + TABLE_NAME + ".token = ticket.id_site_customer " +
                    "where $field1 = ? and $field2 = ?";*/

    //language=sql
    protected final String SQL_SELECT_ALL_SITE_USER = "select * from " + TABLE_NAME;

    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD_EQ =
            "select * from " + TABLE_NAME + " " +
                    "where $field = ?";
    //language=sql
    protected final String SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ =
            "select * from " + TABLE_NAME + " " +
                    "where $field1 = ? and $field2 = ?";

    //language=sql
    protected final String SQL_UPDATE_SET_BOOLEAN_FIELD_TO_WHERE_TOKEN_EQ =
            "update " + TABLE_NAME + " set $field = ? where token = ?";


    protected final String LIMIT_1 = "Limit 1";

    public SiteUserRepositoryOnJDBC(String url, String user, String password) {
        super(url, user, password);
    }

    protected abstract String getTABLE_NAME();

    @Override
    public Optional<T> find(String token) {
        return findFirstSiteUserWhereFieldEq("token", token);
    }

    @Override
    public List<T> getAll() {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_SITE_USER);
            ResultSet resultSet = statement.executeQuery();

            List<T> list = getListFromResultSet(resultSet, rowMapper);
            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<T> findSiteUserOptByEmailAndPassword(String email, String password) {
        return findFirstSiteUserWhereField1EqField2Eq("email", email, "password", password);
    }

    @Override
    public Optional<T> findSiteUserOptByEmail(String email) {
        return findFirstSiteUserWhereFieldEq("email", email);
    }

    @Override
    public Optional<T> findSiteUserOptByPhone(String phone) {
        return findFirstSiteUserWhereFieldEq("phone_number", phone);
    }


    protected List<T> getListSiteUsersWhereFieldEq(String fieldName, String value) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName));
            statement.setString(1, value);
            ResultSet resultSet = statement.executeQuery();

            List<T> listOfSimpleSiteUsers = getListFromResultSet(resultSet, rowMapper);

            return listOfSimpleSiteUsers;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<T> findFirstSiteUserWhereFieldEq(String fieldName, String value) {
        if (value == null) {
            log.warn(fieldName + " is null");
            return Optional.empty();
        }
        try (Connection connection = getConnection()) {
            log.debug("fieldName " + fieldName + " value " + value);
            PreparedStatement statement = connection
                    .prepareStatement(//todo
                            SQL_SELECT_WHERE_FIELD_EQ
                                    .replaceFirst("\\$field", fieldName) +
                                    " " + LIMIT_1);

            setStringOrUUID(statement, 1, value);
            ResultSet resultSet = statement.executeQuery();
            List<T> listOfSimpleSiteUsers = getListFromResultSet(resultSet, rowMapper);

            return listOfSimpleSiteUsers.stream()
                    .findFirst();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    protected List<T> getListSiteUsersWhereField1EqField2Eq(String field1Name, String value1,
                                                            String field2Name, String value2) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ
                                    .replaceFirst("\\$field1", field1Name)
                                    .replaceFirst("\\$field2", field2Name));
            statement.setString(1, value1);
            statement.setString(2, value2);
            ResultSet resultSet = statement.executeQuery();

            List<T> listOfSimpleSiteUsers = getListFromResultSet(resultSet, rowMapper);

            return listOfSimpleSiteUsers;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected Optional<T> findFirstSiteUserWhereField1EqField2Eq(String field1Name, String value1,
                                                                 String field2Name, String value2) {
        log.debug("im finding: " + field1Name + " = " + value1 + ", " + field2Name + " = " + value2);
        if (value1 == null || value2 == null || field1Name == null || field2Name == null) {
            return Optional.empty();
        }
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    .prepareStatement(
                            SQL_SELECT_WHERE_FIELD1_EQ_FIELD2_EQ
                                    .replaceFirst("\\$field1", field1Name)
                                    .replaceFirst("\\$field2", field2Name) +
                                    " " + LIMIT_1);
            statement.setString(1, value1);
            statement.setString(2, value2);
            ResultSet resultSet = statement.executeQuery();

            List<T> listOfSimpleSiteUsers = getListFromResultSet(resultSet, rowMapper);

            return listOfSimpleSiteUsers.stream()
                    .findFirst();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    protected boolean updateBooleanFieldTo(String token, String fieldName, Boolean to) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection
                    //fixme rewrite replace to replacefirst
                    .prepareStatement(
                            SQL_UPDATE_SET_BOOLEAN_FIELD_TO_WHERE_TOKEN_EQ
                                    .replaceFirst("\\$field", fieldName));
            statement.setBoolean(1, to);
            statement.setString(2, token);
            int amount = statement.executeUpdate();
            if (amount == 1) {
                return true;
            } else if (amount == 0) {
                return false;
            } else {
                throw new RuntimeException("collisiums");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deleteBy(String uuid) {
        return false;
        //todo
    }

    //fixme set this into all places
    private void setStringOrUUID(PreparedStatement statement, int i, String value) {
        try {
            if (UUID.fromString(value).toString().equals(value)) {
                statement.setObject(i, UUID.fromString(value));
            } else {
                statement.setString(i, value);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract Logger getLogger();
}
