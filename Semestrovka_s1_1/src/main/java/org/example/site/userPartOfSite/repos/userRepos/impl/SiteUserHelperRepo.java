package org.example.site.userPartOfSite.repos.userRepos.impl;

import lombok.extern.log4j.Log4j2;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.userPartOfSite.models.humans.siteUsers.absClasses.SiteUser;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteCustomerRepository;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteUserRepository;
import org.example.site.userPartOfSite.repos.userRepos.interfaces.SiteWorkerRepository;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.helpers.SiteUserHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
//todo remove to secService
public class SiteUserHelperRepo implements SiteUserRepository<SiteUser> {

    protected SiteCustomerRepository siteUserCustomerCRUDRepository;
    protected SiteWorkerRepository siteWorkerCRUDRepository;

    public SiteUserHelperRepo() {
        this.siteUserCustomerCRUDRepository = MyContext.siteUserCustomerCRUDRepository;
        this.siteWorkerCRUDRepository = MyContext.siteWorkerCRUDRepository;
    }


    @Override
    public String add(SiteUser siteUser) {
        String res = null;
        switch (siteUser.getRole()) {
            case SIMPLE_CUSTOMER -> res = siteUserCustomerCRUDRepository.add((SiteCustomer) siteUser);
            case ADMIN, ROUTER, SUPER -> res = siteWorkerCRUDRepository.add((SiteWorker) siteUser);
        }
        return res;
    }

    @Override
    public Optional<SiteUser> find(String reqToken) {
        Optional<SiteCustomer> siteUserCustomerOptional = siteUserCustomerCRUDRepository.find(reqToken);
        Optional<SiteWorker> siteWorkerOptional = siteWorkerCRUDRepository.find(reqToken);

        return findOptFrom2Opt(siteUserCustomerOptional, siteWorkerOptional);
    }

    @Override
    public List<SiteUser> getAll() {
        List<SiteUser> siteUsers = new ArrayList<>();
        siteUsers.addAll(siteUserCustomerCRUDRepository.getAll());
        siteUsers.addAll(siteWorkerCRUDRepository.getAll());
        return siteUsers;
    }

    @Override
    public boolean update(SiteUser siteUser) {
        boolean updated = false;
        switch (siteUser.getRole()) {
            case SIMPLE_CUSTOMER -> updated = siteUserCustomerCRUDRepository.update((SiteCustomer) siteUser);
            case ADMIN, ROUTER, SUPER -> updated = siteWorkerCRUDRepository.update((SiteWorker) siteUser);
        }
        return updated;
    }

    @Override
    public boolean deleteBy(String reqToken) {
        Optional<SiteUser> siteUserOpt = find(reqToken);
        boolean deleted = false;

        if (siteUserOpt.isPresent()) {
            SiteUser siteUser = siteUserOpt.get();
            if (siteUser instanceof SiteCustomer) {
                deleted = siteUserCustomerCRUDRepository.deleteBy(reqToken);
            } else if (siteUser instanceof SiteWorker) {
                deleted = siteWorkerCRUDRepository.deleteBy(reqToken);
            }
        }
        return deleted;
    }

    @Override
    public Optional<SiteUser> findSiteUserOptByEmailAndPassword(String email, String password) {
        Optional<SiteCustomer> siteUserCustomerOptional = siteUserCustomerCRUDRepository.findSiteUserOptByEmailAndPassword(email, password);
        Optional<SiteWorker> siteWorkerOptional = siteWorkerCRUDRepository.findSiteUserOptByEmailAndPassword(email, password);

        return findOptFrom2Opt(siteUserCustomerOptional, siteWorkerOptional);
    }

    @Override
    public Optional<SiteUser> findSiteUserOptByEmail(String email) {
        Optional<SiteCustomer> siteUserCustomerOptional = siteUserCustomerCRUDRepository.findSiteUserOptByEmail(email);
        Optional<SiteWorker> siteWorkerOptional = siteWorkerCRUDRepository.findSiteUserOptByEmail(email);

        return findOptFrom2Opt(siteUserCustomerOptional, siteWorkerOptional);
    }

    @Override
    public Optional<SiteUser> findSiteUserOptByPhone(String phone) {
        Optional<SiteCustomer> siteUserCustomerOptional = siteUserCustomerCRUDRepository.findSiteUserOptByPhone(phone);
        Optional<SiteWorker> siteWorkerOptional = siteWorkerCRUDRepository.findSiteUserOptByPhone(phone);

        return findOptFrom2Opt(siteUserCustomerOptional, siteWorkerOptional);
    }


    private Optional<SiteUser> findOptFrom2Opt(Optional<SiteCustomer> siteUserCustomerOptional, Optional<SiteWorker> siteWorkerOptional) {
        return SiteUserHelper.findOptFrom2Opt(siteUserCustomerOptional, siteWorkerOptional, "same token in diff tables");
    }
}
