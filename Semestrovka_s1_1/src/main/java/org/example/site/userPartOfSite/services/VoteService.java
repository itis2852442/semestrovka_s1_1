/*
package org.example.services;

import org.example.utils.context.MyContext;
import org.example.utils.helpers.Helper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class VoteService {

    public void addVoteWithAllCheckings(HttpServletRequest req, String candidateName) {
        String tokenFromReq = null;

        Optional<Cookie> cookieWithTokenOptional = Helper.findCookieWithName(req.getCookies(), "token");

        if (cookieWithTokenOptional.isPresent() && (tokenFromReq = cookieWithTokenOptional.get().getValue()) != null) {
            //check if user with that token exists, and token is oneAndOnlyOne
            MyContext.siteUserRepository.checkIfTokenOneAndOnlyOneInDB(tokenFromReq);

            //adding vote to repo
            int changedRows = MyContext.voteRepository.add(new Vote(candidateName, tokenFromReq));

            if (changedRows == 0) {
                System.out.println("voteService; addVoteWithAllCheckings; user with that token has already voted");
            }

            MyContext.siteUserRepository.setVotedTrueToExistingSiteUserWithAllChecks(tokenFromReq);

        }
    }

}
*/
