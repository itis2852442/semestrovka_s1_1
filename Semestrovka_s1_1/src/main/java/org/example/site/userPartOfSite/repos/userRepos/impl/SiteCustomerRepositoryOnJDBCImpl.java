package org.example.site.userPartOfSite.repos.userRepos.impl;


import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.repos.userRepos.absClasses.SiteCustomerRepositoryOnJDBC;
import org.example.site.utils.helpers.Helper;

import java.sql.*;
import java.util.UUID;

@Log4j2
public class SiteCustomerRepositoryOnJDBCImpl extends SiteCustomerRepositoryOnJDBC {

    //language=sql
    private static final String SQL_INSERT_SITE_USER_PREP =
            "insert into site_customer(" +
                    "name, " +
                    "surname, " +
                    "patronymic, " +
                    "age, " +
                    "password, " +
                    "banned, " +
                    "role, " +
                    "passport_country, " +
                    "passport_digits, " +
                    "phone_number, " +
                    "email " +
                    ") values (?,?,?,?,?,?,?,?,?,?,?)";

    //language=sql
    private static final String SQL_UPDATE_WHERE_TOKEN_EQ =
            "update site_customer " +
                    "set name = ?, " +
                    "surname = ?, " +
                    "patronymic = ?, " +
                    "age = ?, " +
                    "password = ?, " +
                    "banned = ?, " +
                    "role = ?, " +
                    "passport_country = ?, " +
                    "passport_digits = ?, " +
                    "phone_number = ?, " +
                    "email = ? " +
                    "where token = ?";


    /*@Override
    public Logger getLogger() {
        return LogManager.getLogger(SiteCustomerRepositoryOnJDBCImpl.class);
    }*/



    public SiteCustomerRepositoryOnJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    private void putSiteCustomerToInsertPrepStatement(PreparedStatement statement, SiteCustomer siteCustomer) throws SQLException {
        //log.debug("siteCustomer.getDateOfBirth(): " + siteCustomer.getDateOfBirth());
        statement.setString(1, siteCustomer.getFirstName());
        statement.setString(2, siteCustomer.getLastName());
        statement.setString(3, siteCustomer.getPatronymic());
        //fixme date
        //log.debug("DATE: " + siteCustomer.getDateOfBirth().toString());
        statement.setInt(4, siteCustomer.getAge());
        statement.setString(5, siteCustomer.getPassword());
        statement.setBoolean(6, siteCustomer.isBanned());
        statement.setString(7, siteCustomer.getRole().toString());
        statement.setString(8, siteCustomer.getPassport().getCountry().toString());
        statement.setString(9, siteCustomer.getPassport().getDigits());
        statement.setString(10, siteCustomer.getPhoneNumber());
        statement.setString(11, siteCustomer.getEmail());
    }

    private void putSiteCustomerToUpdatePrepStatement(PreparedStatement statement, SiteCustomer siteCustomer) throws SQLException {
        //log.debug("siteCustomer.getDateOfBirth(): " + siteCustomer.getDateOfBirth());
        statement.setString(1, siteCustomer.getFirstName());
        statement.setString(2, siteCustomer.getLastName());
        statement.setString(3, siteCustomer.getPatronymic());
        //fixme date
        //log.debug("DATE: " + siteCustomer.getDateOfBirth().toString());
        statement.setInt(4, siteCustomer.getAge());
        statement.setString(6, siteCustomer.getPassword());
        statement.setBoolean(7, siteCustomer.isBanned());
        statement.setString(8, siteCustomer.getPassport().getCountry().toString());
        statement.setString(9, siteCustomer.getPassport().getDigits());
        statement.setString(10, siteCustomer.getPhoneNumber());
        statement.setString(11, siteCustomer.getEmail());
    }

    @Override
    public String add(SiteCustomer siteCustomer) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SITE_USER_PREP, Statement.RETURN_GENERATED_KEYS);
            putSiteCustomerToInsertPrepStatement(statement, siteCustomer);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 1) {
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    log.debug("TRYING TO GET UUID FROM DB: ");
                    if (generatedKeys.next()) {
                        return (generatedKeys.getString(1));
                    }
                    else {
                        throw new SQLException("failed: no UUID");
                    }
                }
            } else if (affectedRows == 0) {
                return null;
            } else {
                throw new RuntimeException("collisiums");
            }

        } catch (SQLException e) {
//                System.out.println("###");
//                System.out.println(e.getErrorCode());
//                System.out.println("###");
//                throw new RuntimeException(e);
            Helper.printSQLException(e);
            if(e.getErrorCode()==0){
                return null;
            }
            else{
                throw new RuntimeException(e);
            }
        }
    }






    /*public void setVotedTrueToExistingSiteUserWithAllChecks(String token) {

        Optional<SiteCustomer> siteUserOptional = getSiteUserOptByToken(token);
        if (siteUserOptional.isPresent()) {
            SiteCustomer simpleSiteUser = siteUserOptional.get();

            simpleSiteUser.setVotedTrue();
            //updating simpleSiteUser in db, that he has voted
            int changedRowsUpdating = update(simpleSiteUser);
            //checking
            if (changedRowsUpdating > 0) {
                Optional<SiteCustomer> siteUserOptAfterUpdating = getSiteUserOptByToken(token);
                if (siteUserOptAfterUpdating.isPresent()) {
                    SiteCustomer simpleSiteUserAfterUpdating = siteUserOptAfterUpdating.get();

                    if (!simpleSiteUserAfterUpdating.isVoted()) {
                        throw new RuntimeException("added vote to voteRepo, but simpleSiteUser.isVoted==false");
                    }
                }
            } else {
                throw new RuntimeException("updating simpleSiteUser crashed");
            }
        }
    }*/


    /*public void checkIfTokenOneAndOnlyOneInDB(String token) {
        int len = getListSiteUsersWhereFieldEq("token", token).size();
        if (len != 1) {
            throw new RuntimeException("token not OneAndOnlyOne");
        }
    }*/


    /*@Override
    public boolean isSiteUserVoted(String token) {
        List<SiteCustomer> list = getListSiteUsersWhereFieldEq("token", token);
        if (list.size() == 1) {
            return list.get(0).isVoted();
        } else if (list.size() > 1) {
            throw new RuntimeException("collisiums");
        } else {
            return false;
        }
    }*/


    /*private boolean systemHasEmail(String email) {
        List<SiteCustomer> list = getListSiteUsersWhereFieldEq("email", email);
        if (list.size() > 1) {
            throw new RuntimeException("collisiums");
        } else if (list.size() == 1) {
            return true;
        } else {
            return false;
        }
    }*/

    @Override
    public boolean update(SiteCustomer siteCustomerToInsert) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_WHERE_TOKEN_EQ);
            putSiteCustomerToUpdatePrepStatement(statement, siteCustomerToInsert);
            int amount = statement.executeUpdate();
            if (amount == 1) {
                return true;
            } else if (amount == 0) {
                return false;
            } else {
                throw new RuntimeException("collisiums");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    protected Logger getLogger() {
        return log;
    }
}
