package org.example.site.userPartOfSite.repos.mediaRepo;

import org.example.site.userPartOfSite.models.media.Comment;
import org.example.site.userPartOfSite.repos.JDBCRepository;
import org.example.site.userPartOfSite.repos.mediaRepo.interfaces.CommentRepo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class CommentRepoJDBCImpl extends JDBCRepository<Comment> implements CommentRepo {

    public CommentRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    public String add(Comment comment) {
        return null;
    }

    @Override
    public Optional<Comment> find(String id) {
        return Optional.empty();
    }

    @Override
    public List<Comment> getAll() {
        return null;
    }

    @Override
    public boolean update(Comment comment) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
