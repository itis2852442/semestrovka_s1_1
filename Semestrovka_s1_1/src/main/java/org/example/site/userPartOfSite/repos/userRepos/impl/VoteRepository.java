/*
package org.example.repos.userRepos.impl;

import org.example.models.Vote;
import org.example.repos.JDBCRepository;
import org.example.repos.userRepos.interfaces.VoteCRUDRepository;
import org.example.utils.mappers.VoteRowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class VoteRepository extends JDBCRepository<Vote> implements VoteCRUDRepository {

    //language=sql
    private static final String SQL_SELECT_ALL_VOTE = "select * from vote";
    //language=sql
    private static final String SQL_INSERT_VOTE_PREP = "insert into vote(candidate_name, site_user_token) values (?,?)";
    //language=sql
    private static final String SQL_SELECT_ALL_VOTE_WHERE_FIELD_EQ_PREP = "select * from vote where $field = ?";
    //language=sql
    private static final String SQL_UPDATE_WHERE_TOKEN_EQ = "update vote set candidate_name = ?, site_user_token = ? where site_user_token = ?";


    public VoteRepository(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    public int add(Vote voteToInsert) {
        String methodName = "add()";

        try (Connection connection = getConnection()) {
            int amountOfVotesWithTokenInDB = this.getAllWhereSiteUserTokenEq(voteToInsert.getSiteUserToken()).size();
            System.out.println(getClass().getName() + "; " + methodName + "; " + "amountOfVotesWithTokenInDB = " + amountOfVotesWithTokenInDB);
            if (amountOfVotesWithTokenInDB == 1) {
                return 0;
            }
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_VOTE_PREP);
            statement.setString(1, voteToInsert.getCandidateName());
            statement.setString(2, voteToInsert.getSiteUserToken());
            int amount = statement.executeUpdate();
            return amount;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Vote> getAll() {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_VOTE);
            ResultSet resultSet = statement.executeQuery();

            return getListFromResultSet(resultSet, new VoteRowMapper());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Vote> getAllWhereFieldEq(String fieldName, String filedValue) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_VOTE_WHERE_FIELD_EQ_PREP.replace("$field", fieldName));
            statement.setString(1, filedValue);
            ResultSet resultSet = statement.executeQuery();

            List<Vote> list = getListFromResultSet(resultSet, new VoteRowMapper());
            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Vote> getAllWhereCandidateNameEq(String candidateName) {
        return getAllWhereFieldEq("candidate_name", candidateName);
    }

    @Override
    public List<Vote> getAllWhereSiteUserTokenEq(String token) {
        return getAllWhereFieldEq("site_user_token", token);
    }

    @Override
    public int update(Vote vote) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_WHERE_TOKEN_EQ);
            statement.setString(1, vote.getCandidateName());
            statement.setString(2, vote.getSiteUserToken());
            statement.setString(3, vote.getSiteUserToken());
            int amount = statement.executeUpdate();
            return amount;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
*/
