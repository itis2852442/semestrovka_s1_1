package org.example.site.userPartOfSite.repos;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CRUDRepository<T> {

    String add(T t);

    Optional<T> find(String id);

    List<T> getAll();

    boolean update(T t);

    boolean deleteBy(String id);


}
