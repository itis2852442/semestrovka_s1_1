package org.example.site.userPartOfSite.repos.mediaRepo.interfaces;

import org.example.site.userPartOfSite.models.media.Comment;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface CommentRepo extends CRUDRepository<Comment> {
}
