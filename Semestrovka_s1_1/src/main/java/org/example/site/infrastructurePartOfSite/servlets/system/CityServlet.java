package org.example.site.infrastructurePartOfSite.servlets.system;

import com.google.gson.Gson;
import org.example.site.utils.enums.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "CityServlet", urlPatterns = "/infr/system/city")
public class CityServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("json/text");
        List<String> res = Arrays.stream(Country.City.values()).map(Enum::toString).toList();
        resp.getWriter().println(new Gson().toJson(res));
    }
}
