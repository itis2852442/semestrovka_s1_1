package org.example.site.infrastructurePartOfSite.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Path {

    private Airport from;
    private Airport to;
//    private int distance;

}
