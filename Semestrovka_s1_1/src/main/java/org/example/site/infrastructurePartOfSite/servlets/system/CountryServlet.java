package org.example.site.infrastructurePartOfSite.servlets.system;

import com.google.gson.Gson;
import org.example.site.utils.enums.Country;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "CountryServlet", urlPatterns = "/infr/system/country")
public class CountryServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("json/text");
        List<String> res = Arrays.stream(Country.values()).map(Enum::toString).toList();
        resp.getWriter().println(new Gson().toJson(res));
    }
}
