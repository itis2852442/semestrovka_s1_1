package org.example.site.infrastructurePartOfSite.servlets.system;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.example.site.utils.context.MyContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CountryCityServlet", urlPatterns = "/infr/system/countryCity")
@Log4j2
public class CountryCityServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> res = MyContext.countryCityRepoJDBCImpl.getAll();
        resp.setContentType("json/text");
        resp.getWriter().println(new Gson().toJson(res));
    }
}
