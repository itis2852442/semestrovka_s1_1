package org.example.site.infrastructurePartOfSite.servlets.superr;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.exceptions.ServiceConflictException;
import org.example.site.utils.exceptions.ServiceException;
import org.example.site.utils.exceptions.ServiceNotCorrectInputException;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.locations.Locations;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashMap;

@WebServlet(name = "AddCountryCityServlet", urlPatterns = "/infr/superr/addCountryCity")
public class AddCountryCityServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Template template = FreemarkerTemplates.getSuperrAddCountryCity();
        HashMap<String, Object> map = new HashMap<>();
        resp.setContentType("text/html");
        try {
            template.process(map, resp.getWriter());
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String country = req.getParameter("country");
        String city = req.getParameter("city");

        try {
            MyContext.countryCityInfrService.addWithCheck(country,city);
            //here im redirecting client on server side
            resp.sendRedirect(Locations.getSuperrAddCountryCity());
        } catch (Exception e) { //(as n lgt)
            if(e instanceof ServiceNotCorrectInputException){
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
            else if(e instanceof ServiceConflictException){
                resp.sendError(HttpServletResponse.SC_CONFLICT, e.getMessage());
            }
            else{
                throw new RemoteException("", e);
            }
        }
    }

}
