package org.example.site.infrastructurePartOfSite.repo.interfaces;

import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface AirportRepo extends CRUDRepository<Airport> {
}
