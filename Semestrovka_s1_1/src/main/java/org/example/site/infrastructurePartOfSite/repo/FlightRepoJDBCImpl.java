package org.example.site.infrastructurePartOfSite.repo;

import org.example.site.infrastructurePartOfSite.repo.interfaces.FlightRepo;
import org.example.site.infrastructurePartOfSite.models.Aircraft;
import org.example.site.infrastructurePartOfSite.models.Flight;
import org.example.site.userPartOfSite.repos.JDBCRepository;

import java.util.List;
import java.util.Optional;

public class FlightRepoJDBCImpl extends JDBCRepository<Aircraft> implements FlightRepo {

    public FlightRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    public String add(Flight flight) {
        return null;
    }

    @Override
    public Optional<Flight> find(String id) {
        return Optional.empty();
    }

    @Override
    public List<Flight> getAll() {
        return null;
    }

    @Override
    public boolean update(Flight flight) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
