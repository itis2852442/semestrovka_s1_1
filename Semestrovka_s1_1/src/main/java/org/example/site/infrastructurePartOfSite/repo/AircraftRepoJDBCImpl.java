package org.example.site.infrastructurePartOfSite.repo;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.infrastructurePartOfSite.models.Aircraft;
import org.example.site.infrastructurePartOfSite.repo.interfaces.AircraftRepo;
import org.example.site.userPartOfSite.repos.JDBCRepositoryWithSQLTemps;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.helpers.Helper;
import org.example.site.utils.mapper.AircraftRowMapper;

import java.sql.*;
import java.util.List;

@Log4j2
public class AircraftRepoJDBCImpl extends JDBCRepositoryWithSQLTemps<Aircraft> implements AircraftRepo {

    public AircraftRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    protected Logger getLogger() {
        return log;
    }

    @Override
    protected RowMapper<Aircraft> getRowMapper() {
        return new AircraftRowMapper();
    }

    @Override
    protected String getTABLE() {
        return "aircraft";
    }

    @Override
    protected String getPK() {
        return "on_board_number";
    }

    @Override
    protected String getFIELDS() {
        return "(distro, model, age, max_capacity, amount_of_seats)";
    }

    @Override
    protected String getFIELDSQ() {
        return "(?, ?, ?, ?, ?)";
    }

    @Override
    protected void putToPrepStat(PreparedStatement statement, Aircraft aircraft) {
        try {
            statement.setString(1, String.valueOf(aircraft.getDistro()));
            statement.setString(2, String.valueOf(aircraft.getModel()));
            statement.setString(3, String.valueOf(aircraft.getAge()));
            statement.setString(4, String.valueOf(aircraft.getMaxCapacity()));
            statement.setString(5, String.valueOf(aircraft.getAmountOfSeats()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public String add(Aircraft aircraft) {
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("insert into distro_model_for_aircrafts(distro, model) VALUES (?, ?)");
            statement.setString(1, String.valueOf(aircraft.getDistro()));
            statement.setString(2, String.valueOf(aircraft.getModel()));

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                return null;
            } else if(affectedRows>1) {
                throw new RuntimeException("collisiums");
            }

        } catch (SQLException e) {
            Helper.printSQLException(e);
            if(e.getErrorCode()==0){
                return null;
            }
            else{
                throw new RuntimeException(e);
            }
        }

        return super.add(aircraft);
    }



    @Override
    public boolean update(Aircraft aircraft) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
