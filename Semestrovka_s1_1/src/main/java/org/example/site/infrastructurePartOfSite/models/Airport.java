package org.example.site.infrastructurePartOfSite.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.example.site.utils.enums.Country;

import java.util.UUID;

@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Airport {

    private UUID uuid;
    private String name;
    private Country country;
    private Country.City city;

}
