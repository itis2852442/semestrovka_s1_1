package org.example.site.infrastructurePartOfSite.repo.interfaces;

import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface TicketInfrRepo extends CRUDRepository<Ticket> {
}
