package org.example.site.infrastructurePartOfSite.repo;

import org.apache.logging.log4j.Logger;
import org.example.site.infrastructurePartOfSite.models.Flight;
import org.example.site.infrastructurePartOfSite.repo.interfaces.FlightInfrRepo;
import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.userPartOfSite.repos.JDBCRepository;
import org.example.site.userPartOfSite.repos.JDBCRepositoryWithSQLTemps;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.enums.Country;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class FlightInfrRepoJDBCImpl extends JDBCRepositoryWithSQLTemps<Flight> implements FlightInfrRepo {

    public FlightInfrRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    protected Logger getLogger() {
        return null;
    }

    @Override
    protected RowMapper<Flight> getRowMapper() {
        return null;
    }

    @Override
    protected String getTABLE() {
        return null;
    }

    @Override
    protected String getPK() {
        return null;
    }

    @Override
    protected String getFIELDS() {
        return null;
    }

    @Override
    protected String getFIELDSQ() {
        return null;
    }

    @Override
    protected void putToPrepStat(PreparedStatement statement, Flight flight) {

    }


    public List<Flight> findAvailiableFlightsFromCityToCityWhen(Country.City from, Country.City to, Date dateStart, Date dateEnd){
        //fixme!!!!
        return List.of(Flight.builder()
                        .uuid(UUID.randomUUID())
                .build(), new Flight(), new Flight());
    }

    public List<Ticket> findAvailiableTicketsForFlight(UUID flightUUID){
        //fixme!!!!
        return new ArrayList<>();
    }

    public List<Flight> findFlights(Country.City from, Country.City to, Date startDate, Date endDate){
        try(Connection connection = getConnection()){
            PreparedStatement statement = connection.prepareStatement("");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        //todo
        return new ArrayList<>();
    }



    @Override
    public boolean update(Flight flight) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
