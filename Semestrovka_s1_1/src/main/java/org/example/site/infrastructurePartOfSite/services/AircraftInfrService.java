package org.example.site.infrastructurePartOfSite.services;

import org.example.site.infrastructurePartOfSite.models.Aircraft;
import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.AircraftDistro;
import org.example.site.utils.enums.AircraftModel;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceException;

import java.util.UUID;

public class AircraftInfrService {

    private UUID add(AircraftDistro distro, AircraftModel model, int age, int maxCapacity, int amountOfSeats){
        Aircraft aircraft = Aircraft.builder()
                .distro(distro)
                .model(model)
                .age(age)
                .maxCapacity(maxCapacity)
                .amountOfSeats(amountOfSeats)
                .build();
        String UUIDStr = MyContext.aircraftRepoJDBC.add(aircraft);
        if(UUIDStr==null){
            return null;
        }else{
            return UUID.fromString(UUIDStr);
        }
    }

    public UUID addWithChecks(String distro, String model, String age, String maxCapacity, String amountOfSeats) throws Exception{
        check(distro,  model, age, maxCapacity, amountOfSeats);
        UUID res = add(AircraftDistro.valueOf(distro),  AircraftModel.valueOf(model), Integer.parseInt(age), Integer.parseInt(maxCapacity), Integer.parseInt(amountOfSeats));
        if(res==null){
            throw new ServiceException("not added");
        }
        return res;
    }

    private boolean check(String distro, String model, String age, String maxCapacity, String amountOfSeats) throws Exception{
        try{
            AircraftDistro.valueOf(distro);
        } catch (Exception e) {
            throw new ServiceException("incorrect country", e);
        }

        try{
            AircraftModel.valueOf(model);
        } catch (Exception e){
            throw new ServiceException("incorrect city", e);
        }

        try{
            Integer res = Integer.parseInt(age);
        } catch (Exception e) {
            throw new ServiceException("incorrect age", e);
        }

        try{
            Integer res = Integer.parseInt(maxCapacity);
        } catch (Exception e) {
            throw new ServiceException("incorrect maxCapacity", e);
        }

        try{
            Integer res = Integer.parseInt(amountOfSeats);
        } catch (Exception e) {
            throw new ServiceException("incorrect amountOfSeats", e);
        }



        if(Integer.parseInt(age)<1){
            throw new ServiceException("incorrect age");
        }
        if(Integer.parseInt(maxCapacity)<500){
            throw new ServiceException("incorrect maxCapacity");
        }
        if(Integer.parseInt(amountOfSeats)<5){
            throw new ServiceException("incorrect amountOfSeats");
        }

        return true;
    }


}
