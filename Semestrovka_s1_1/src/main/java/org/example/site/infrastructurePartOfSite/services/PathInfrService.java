package org.example.site.infrastructurePartOfSite.services;

import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.infrastructurePartOfSite.models.Path;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceException;

import java.util.Optional;
import java.util.UUID;

public class PathInfrService {

    private UUID add(Airport from, Airport to){
        Path path = Path.builder()
                .from(from)
                .to(to)
                .build();
        String UUIDStr = MyContext.pathRepoJDBC.add(path);
        if(UUIDStr==null){
            return null;
        }else{
            return UUID.fromString(UUIDStr);
        }
    }

    public UUID addWithChecks(String fromAirportId, String toAirportId) throws Exception{
        //check(fromCountry, fromCity, toCountry, toCity);

        Optional<Airport> airportOptional1 = MyContext.airportRepoJDBC.find(fromAirportId);
        Optional<Airport> airportOptional2 = MyContext.airportRepoJDBC.find(toAirportId);

//        Optional<Airport> airport1 = MyContext.airportRepoJDBC.getListWhereCountryEqCityEqNameEq(Country.valueOf(fromCountry), Country.City.valueOf(fromCity), fromName).stream()
//                .findFirst();
//        Optional<Airport> airport2 = MyContext.airportRepoJDBC.getListWhereCountryEqCityEqNameEq(Country.valueOf(toCountry), Country.City.valueOf(toCity), toName).stream()
//                .findFirst();
        UUID res = null;
        if(airportOptional1.isPresent() && airportOptional2.isPresent()){
            res = add(airportOptional1.get(), airportOptional2.get());
        }
        if(res==null){
            throw new ServiceException("not added");
        }
        return res;

    }

    private boolean check(String fromCountry, String fromCity, String toCountry, String toCity) throws Exception{

        boolean r1 = MyContext.countryCityInfrService.check(fromCountry, fromCity);
        boolean r2 = MyContext.countryCityInfrService.check(toCountry, toCity);

        return r1 && r2;
    }

}
