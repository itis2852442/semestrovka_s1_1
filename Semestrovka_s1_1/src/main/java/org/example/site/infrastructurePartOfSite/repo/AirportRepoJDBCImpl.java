package org.example.site.infrastructurePartOfSite.repo;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.infrastructurePartOfSite.repo.interfaces.AirportRepo;
import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.userPartOfSite.repos.JDBCRepositoryWithSQLTemps;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.enums.Country;
import org.example.site.utils.helpers.Helper;
import org.example.site.utils.mapper.AirportRawMapper;

import java.sql.*;
import java.util.List;

@Log4j2
public class AirportRepoJDBCImpl extends JDBCRepositoryWithSQLTemps<Airport> implements AirportRepo {


    public AirportRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    protected Logger getLogger() {
        return log;
    }

    @Override
    protected RowMapper<Airport> getRowMapper() {
        return new AirportRawMapper();
    }

    @Override
    protected String getTABLE() {
        return "airport";
    }

    @Override
    protected String getPK() {
        return "id";
    }

    @Override
    protected String getFIELDS() {
        return "(name, country, city)";
    }

    @Override
    protected String getFIELDSQ() {
        return "(?, ?, ?)";
    }

    @Override
    protected void putToPrepStat(PreparedStatement statement, Airport airport) {
        try {
            statement.setString(1,airport.getName());
            statement.setString(2, String.valueOf(airport.getCountry()));
            statement.setString(3, String.valueOf(airport.getCity()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    public List<Airport> getListWhereCountryEqCityEqNameEq(Country country, Country.City city, String name){
        return getListWhereField1EqField2EqField3Eq("country", country.toString(), "city", city.toString(), "name", name);
    }


    @Override
    public boolean update(Airport airport) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
