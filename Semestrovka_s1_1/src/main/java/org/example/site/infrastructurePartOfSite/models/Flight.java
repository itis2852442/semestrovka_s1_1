package org.example.site.infrastructurePartOfSite.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.sql.Time;
import java.util.Date;
import java.util.UUID;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

    @NonNull
    private UUID uuid;
    @NonNull
    private Path path;
    @NonNull
    private Aircraft aircraft;
    @NonNull
    private Date departureDate;
    @NonNull
    private Time departureTime;

}
