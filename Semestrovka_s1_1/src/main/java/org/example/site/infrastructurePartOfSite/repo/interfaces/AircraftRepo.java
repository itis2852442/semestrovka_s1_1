package org.example.site.infrastructurePartOfSite.repo.interfaces;

import org.example.site.infrastructurePartOfSite.models.Aircraft;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface AircraftRepo extends CRUDRepository<Aircraft> {
}
