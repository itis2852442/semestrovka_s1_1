package org.example.site.infrastructurePartOfSite.services;

import org.example.site.infrastructurePartOfSite.repo.interfaces.TicketInfrRepo;
import org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer.BuyTicketDTO;
import org.example.site.utils.todelete.dto2.response.BuyTicketResponse;
import org.example.site.utils.context.MyContext;

public class TicketInfrService {

    private final TicketInfrRepo ticketInfrRepo = MyContext.ticketInfrRepoJDBCImpl;

    public BuyTicketResponse buyTicketWithChecking(BuyTicketDTO buyTicketDTO){
        if(checkIfPlaceInFlightIsAvailable(buyTicketDTO)){
            return buyTicket(buyTicketDTO);
        }
        else{
            return BuyTicketResponse.builder()
                    .bought(false)
                    .build();
        }
    }

    public boolean checkIfPlaceInFlightIsAvailable(BuyTicketDTO buyTicketDTO){
        //todo!!!
        return true;
    }

    public BuyTicketResponse buyTicket(BuyTicketDTO buyTicketDTO){
        return BuyTicketResponse.builder()
                .bought(true)
                .ticket(MyContext.ticketInfrRepoJDBCImpl.buyTicketsOnFlight(
                        buyTicketDTO.getFlightUUID(),
                        buyTicketDTO.getPlace()))
                .build();
    }





}
