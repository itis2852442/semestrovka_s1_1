package org.example.site.infrastructurePartOfSite.services;

import org.example.site.infrastructurePartOfSite.models.Airport;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceConflictException;
import org.example.site.utils.exceptions.ServiceException;
import org.example.site.utils.exceptions.ServiceNotCorrectInputException;

import java.util.UUID;

public class AirportInfrService {

    private UUID add(String name, Country country, Country.City city){
        Airport airport = Airport.builder()
                .name(name)
                .country(country)
                .city(city)
                .build();
        String UUIDStr = MyContext.airportRepoJDBC.add(airport);
        if(UUIDStr==null){
            return null;
        }else{
            return UUID.fromString(UUIDStr);
        }
    }

    public UUID addWithChecks(String country, String city, String name) throws Exception{
        MyContext.countryCityInfrService.check(country, city);
        check(name);
        UUID res = add(name, Country.valueOf(country), Country.City.valueOf(city));
        if(res==null){
            throw new ServiceConflictException("not added");
        }
        return res;
    }

    private void check(String name) throws Exception{
        if(name.length()<1){
            throw new ServiceNotCorrectInputException("incorrect name");
        }
    }









}
