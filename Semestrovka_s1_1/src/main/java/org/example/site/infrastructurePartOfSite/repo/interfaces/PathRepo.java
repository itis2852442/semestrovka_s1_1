package org.example.site.infrastructurePartOfSite.repo.interfaces;

import org.example.site.infrastructurePartOfSite.models.Path;
import org.example.site.userPartOfSite.repos.CRUDRepository;

public interface PathRepo extends CRUDRepository<Path> {



}
