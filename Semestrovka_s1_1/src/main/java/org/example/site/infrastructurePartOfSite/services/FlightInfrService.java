package org.example.site.infrastructurePartOfSite.services;

import org.example.site.infrastructurePartOfSite.models.Flight;
import org.example.site.utils.todelete.dto2.request.siteUser.siteCustomer.SearchAvailableFlightsFromCityToCityWhenDTO;
import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.utils.context.MyContext;

import java.util.List;
import java.util.UUID;

public class FlightInfrService {

    public List<Flight> findAvaliableFlights(SearchAvailableFlightsFromCityToCityWhenDTO searchAvailableFlightsFromCityToCityWhenDTO){
        return MyContext.flightInfrRepoJDBCImpl.findAvailiableFlightsFromCityToCityWhen(
                searchAvailableFlightsFromCityToCityWhenDTO.getFromCity(),
                searchAvailableFlightsFromCityToCityWhenDTO.getToCity(),
                searchAvailableFlightsFromCityToCityWhenDTO.getDateStart(),
                searchAvailableFlightsFromCityToCityWhenDTO.getDateEnd()
        );
    }

    public List<Ticket> findAvailiableTicketsForFlight(UUID flightUUID){
        return MyContext.flightInfrRepoJDBCImpl.findAvailiableTicketsForFlight(flightUUID);
    }

//    public List<Flight> findFlights(Country.City from, Country.City to, Date startDate, Date endDate){
//        MyContext.flightInfrRepoJDBCImpl.find();
//    }

}
