package org.example.site.infrastructurePartOfSite.repo;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;
import org.example.site.infrastructurePartOfSite.models.Path;
import org.example.site.infrastructurePartOfSite.repo.interfaces.PathRepo;
import org.example.site.userPartOfSite.repos.JDBCRepositoryWithSQLTemps;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.helpers.Helper;
import org.example.site.utils.mapper.PathRowMapper;

import java.sql.*;
import java.util.List;
import java.util.Optional;

@Log4j2
public class PathRepoJDBCImpl extends JDBCRepositoryWithSQLTemps<Path> implements PathRepo {

    public PathRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    @Override
    protected Logger getLogger() {
        return log;
    }

    @Override
    protected RowMapper<Path> getRowMapper() {
        return new PathRowMapper();
    }

    @Override
    protected String getTABLE() {
        return "path";
    }

    @Override
    protected String getPK() {
        return "id";
    }

    @Override
    protected String getFIELDS() {
        return "(departure_airport_id, arrival_airport_id)";
    }

    @Override
    protected String getFIELDSQ() {
        return "(?, ?)";
    }

    @Override
    protected void putToPrepStat(PreparedStatement statement, Path path){
        try {
            statement.setString(1, String.valueOf(path.getFrom().getUuid()));
            statement.setString(2, String.valueOf(path.getTo().getUuid()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }





    @Override
    public boolean update(Path path) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
