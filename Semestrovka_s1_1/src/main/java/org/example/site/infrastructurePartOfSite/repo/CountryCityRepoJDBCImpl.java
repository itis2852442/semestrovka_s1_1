package org.example.site.infrastructurePartOfSite.repo;

import org.example.site.userPartOfSite.repos.JDBCRepository;
import org.example.site.userPartOfSite.utils.mappers.RowMapper;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CountryCityRepoJDBCImpl extends JDBCRepository<String> {

    protected RowMapper<String> rowMapper = rs -> rs.getString(1) + " | " +rs.getString(2);

    public CountryCityRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }

    public List<String> getAll(){
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("select * from country_city_for_airports");
            ResultSet res = preparedStatement.executeQuery();
            List<String> ress = getListFromResultSet(res, rowMapper);
            return ress;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean add(Country country, Country.City city) {
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("insert into country_city_for_airports(country, city) VALUES (?, ?)");
            preparedStatement.setString(1,country.toString());
            preparedStatement.setString(2,city.toString());

            int affRows = preparedStatement.executeUpdate();
            if(affRows==0){
                return false;
            }
            else if(affRows==1){
                return true;
            }
            else{
                throw new RuntimeException("collis");
            }

        } catch (SQLException e) {
            if(e.getErrorCode()==0){
                return false;
            }
            throw new RuntimeException("error code: " + e.getErrorCode(),e);
        }
    }

}
