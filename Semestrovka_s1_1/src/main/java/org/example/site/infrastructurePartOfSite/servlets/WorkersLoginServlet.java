package org.example.site.infrastructurePartOfSite.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.freemarker.FreemarkerUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@WebServlet(name = "WorkersLoginServlet", urlPatterns = "/infr/login")
public class WorkersLoginServlet extends HttpServlet {
//todo delete old cookie when enter

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        //fixme unset cookie
        Template template = FreemarkerTemplates.getWorkerLogin();
        HashMap<String, Object> map = new HashMap<>();
        resp.setContentType("text/html");
        //
//            List<SiteUser> siteUserList = MyContext.siteUserRepository.getAll();
//            map.put("siteUsers",siteUserList);

        try {
            template.process(map, resp.getWriter());
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //todo "post" auth everywhere
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        boolean rememberMe = Boolean.parseBoolean(req.getParameter("rememberMe"));

        //UUID uuid = MyContext.siteUserSecurityService.enterSiteUserToSystem(email, password, rememberMe);

        MyContext.siteWorkerSecurityService.enterSiteUserToSystem(req, resp, email, password, rememberMe);

    }
}
