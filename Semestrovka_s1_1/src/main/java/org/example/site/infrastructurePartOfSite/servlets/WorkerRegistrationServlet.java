package org.example.site.infrastructurePartOfSite.servlets;

//import org.example.model.Credentials;

import lombok.extern.log4j.Log4j2;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.SiteUserRole;
import org.example.site.utils.exceptions.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(name = "WorkerRegistrationServlet", urlPatterns = "/infr/superr/regSiteWorker")
@Log4j2
public class WorkerRegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        log.debug("getLocalAddr: " + req.getLocalAddr());
        log.debug("getLocalPort: " + req.getLocalPort());
        log.debug("getRemotePort: " + req.getRemotePort());
        log.debug("getServerPort: " + req.getServerPort());

//        Template template = FreemarkerUtil.getConfig().getTemplate("reg/regCustomer.ftl");
//        HashMap<String, Object> map = new HashMap<>();
//
//        resp.setContentType("text/html");
//        try {
//            template.process(map, resp.getWriter());
//        } catch (TemplateException e) {
//            throw new RuntimeException(e);
//        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String patronymic = req.getParameter("patronymic");
        String age = req.getParameter("age");
        String role = req.getParameter("role");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        boolean correct = false;
        try {
            correct = MyContext.checkerService.checkSiteWorkerReg(
                    firstName,
                    lastName,
                    patronymic,
                    age,
                    phoneNumber,
                    email,
                    password,
                    role);
        } catch (Exception e) {
            if(e instanceof ServiceException){
                log.debug("send error 400 to client: ");
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                log.debug("set status 400");
                resp.getWriter().println("{\"error: \""+ e.getMessage() +"}");
                //resp.sendError(HttpServletResponse.SC_CONFLICT, e.getMessage());
            }
            else{
                throw new RuntimeException();
            }
        }



        if(correct){
            log.debug("checkSiteCustomerReg: " + correct);
            SiteWorker siteWorkerToReg = SiteWorker.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .patronymic(patronymic)
                    .age(Integer.parseInt(age))
                    .phoneNumber(phoneNumber)
                    .email(email)

                    .password(password)

                    .role(SiteUserRole.valueOf(role))
                    .banned(false)
                    .build();

            UUID uuidOfRegisteredCustomer = MyContext.siteWorkerSecurityService.registerSiteWorkerWithAllChecks(siteWorkerToReg);

            if (uuidOfRegisteredCustomer == null) {
                //there is another user in db
                resp.setStatus(HttpServletResponse.SC_CONFLICT);
                resp.getWriter().println("{\"error: \""+ "user exists" +"}");
//            resp.setContentType("json/text");
//            resp.getWriter().println("{}");
//            resp.sendRedirect(MyContext.locations.getProperty("login"));
            } else {
                //registered
                //todo check in postman
                /*Cookie generatedCookie = new Cookie("token", uuidOfRegisteredCustomer.toString());
                generatedCookie.setMaxAge(2000 * 2000);//todo 2 days
                resp.addCookie(generatedCookie);*/

//                t odo forward login with login and password
                resp.setStatus(HttpServletResponse.SC_OK);

                //log.debug("sending client to: " + MyContext.locations.getProperty("login"));
//                resp.sendRedirect(MyContext.locations.getProperty("login"));
//                resp.sendRedirect("http://localhost:8080/login");
            }
        }




        /*//
        Optional<SiteUser> siteUserOpt = SiteUserHelper.findSiteUserOptByReq(req);
        if(siteUserOpt.isPresent()){
            SiteUser siteUser = siteUserOpt.get();
            if(siteUser instanceof SiteCustomer){
                //String passportDigits = req.getParameter("passportDigits");
                //todo other credentials
                //fixme
            }
            else if(siteUser instanceof SiteWorker){
                MyContext.siteCustomerService.registerSiteCustomer(req, resp, login, password);
            }
        }
        //*/

//        System.out.println(h.getTokenAndCredentialsMap());


        //fixme redirect
        /*Template template = FreemarkerUtil.getConfig().getTemplate("reg/successReg.ftl");
        HashMap<String, Object> map = new HashMap<>();*/

//        List<SiteUser> siteUserList = MyContext.siteUserRepository.getAll();
//        map.put("siteUsers",siteUserList);

        //fixme change to json
        /*resp.setContentType("text/html");
        try {
            template.process(map, resp.getWriter());
        } catch (TemplateException e) {
            throw new RuntimeException(e);
        }*/

//        RequestDispatcher dispatcher = getServletContext()
//                .getRequestDispatcher("http://localhost:8080/task2/login");
//        dispatcher.forward(req, resp);
    }
}
