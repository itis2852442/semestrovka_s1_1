package org.example.site.infrastructurePartOfSite.servlets.system;

import com.google.gson.Gson;
import org.example.site.infrastructurePartOfSite.models.Flight;
import org.example.site.utils.context.MyContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "FlightServlet", urlPatterns = "/infr/system/flights")
public class FlightServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Flight> flightList = MyContext.flightInfrRepoJDBCImpl.getAll();
        resp.setContentType("json/text");
        resp.getWriter().println(new Gson().toJson(flightList));
    }


}
