package org.example.site.infrastructurePartOfSite.models;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.example.site.utils.enums.AircraftDistro;
import org.example.site.utils.enums.AircraftModel;

import java.util.Date;

@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Aircraft {

    private String OnBoardNumber;

    private AircraftDistro distro;
    private AircraftModel model;
    private int age;

    private int maxCapacity;
    private int amountOfSeats;

}
