package org.example.site.infrastructurePartOfSite.servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.log4j.Log4j2;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteCustomer;
import org.example.site.userPartOfSite.models.humans.siteUsers.Impl.SiteWorker;
import org.example.site.utils.context.MyContext;
import org.example.site.utils.freemarker.FreemarkerTemplates;
import org.example.site.utils.helpers.SiteUserHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "InfrHomeServlet", urlPatterns = "/infr/home")
@Log4j2
public class InfrHomeServlet extends HttpServlet {

    /*@Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.siteUserCustomerCRUDRepository = (SiteCustomerRepository) getServletContext().getAttribute("siteUserCustomerCRUDRepository");
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<SiteWorker> siteWorkerOpt = SiteUserHelper.findSiteWorkerFromRequest(req);

        log.trace("user trying to enter to infr home");
        if (siteWorkerOpt.isPresent()) {
            log.trace("user authorized");
            SiteWorker siteWorker = siteWorkerOpt.get();

            // fixme repos
            switch (siteWorker.getRole()) {
                case ADMIN -> {
                    log.trace("user is admin, entering to home");
                    Template template = FreemarkerTemplates.getAdminHome();
                    HashMap<String, Object> map = new HashMap<>();

                    List<SiteCustomer> simpleSiteUserList = MyContext.siteUserCustomerCRUDRepository.getAll();
                    map.put("siteUsers", simpleSiteUserList);

                    resp.setContentType("text/html");
                    try {
                        template.process(map, resp.getWriter());
                    } catch (TemplateException | IOException e) {
                        throw new RuntimeException(e);
                    }
//            resp.getWriter().println(pageContent);
                    //resp.sendRedirect("secretPage.jsp");
                }
                case ROUTER -> {
                    log.trace("user is router, entering to home");
                    Template template = FreemarkerTemplates.getRouterHome();
                    HashMap<String, Object> map = new HashMap<>();

                    List<SiteCustomer> simpleSiteUserList = MyContext.siteUserCustomerCRUDRepository.getAll();
                    map.put("siteUsers", simpleSiteUserList);

                    resp.setContentType("text/html");
                    try {
                        template.process(map, resp.getWriter());
                    } catch (TemplateException | IOException e) {
                        throw new RuntimeException(e);
                    }
//            resp.getWriter().println(pageContent);
                    //resp.sendRedirect("secretPage.jsp");
                }
                case SUPER -> {
                    log.trace("user is superr, entering to home");
                    Template template = FreemarkerTemplates.getSuperrHome();
                    HashMap<String, Object> map = new HashMap<>();

                    List<SiteCustomer> simpleSiteUserList = MyContext.siteUserCustomerCRUDRepository.getAll();
                    map.put("siteUsers", simpleSiteUserList);

                    resp.setContentType("text/html");
                    try {
                        template.process(map, resp.getWriter());
                    } catch (TemplateException | IOException e) {
                        throw new RuntimeException(e);
                    }
//            resp.getWriter().println(pageContent);
                    //resp.sendRedirect("secretPage.jsp");
                }
                default -> {
                    log.trace("user is not worker, forbidden");
                    resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                }
            }
        } else {
            log.trace("user unAuthorized, forbidden");
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
