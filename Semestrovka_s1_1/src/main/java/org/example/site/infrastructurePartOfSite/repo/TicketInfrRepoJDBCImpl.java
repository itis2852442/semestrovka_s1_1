package org.example.site.infrastructurePartOfSite.repo;

import org.example.site.infrastructurePartOfSite.repo.interfaces.TicketInfrRepo;
import org.example.site.userPartOfSite.models.flights.Ticket;
import org.example.site.userPartOfSite.repos.JDBCRepository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class TicketInfrRepoJDBCImpl extends JDBCRepository<Ticket> implements TicketInfrRepo {

    //language=sql
    private static final String SQL_SELECT_WHERE_FIELD_EQ = "select * from ticket where %field = ?";

    public TicketInfrRepoJDBCImpl(String url, String user, String password) {
        super(url, user, password);
    }



    public Ticket buyTicketsOnFlight(UUID flightUUID, int place){
        //todo!!!
        return null;
    }

    @Override
    public String add(Ticket ticket) {
        return null;
    }

    @Override
    public Optional<Ticket> find(String id) {
        return Optional.empty();
    }

    public List<Ticket> findAvaliableTicketsOnFlight(UUID flightUUID){
        //fixme!!!
        return new ArrayList<>();
    }


    @Override
    public List<Ticket> getAll() {
        return null;
    }

    public List<Ticket> getAllWhereFieldEq(String field, UUID value){
        //todo
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(SQL_SELECT_WHERE_FIELD_EQ.formatted(field))) {
            preparedStatement.setString(1, String.valueOf(value));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean update(Ticket ticket) {
        return false;
    }

    @Override
    public boolean deleteBy(String id) {
        return false;
    }
}
