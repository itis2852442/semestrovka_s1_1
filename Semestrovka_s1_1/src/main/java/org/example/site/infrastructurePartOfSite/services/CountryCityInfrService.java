package org.example.site.infrastructurePartOfSite.services;

import org.example.site.utils.context.MyContext;
import org.example.site.utils.enums.Country;
import org.example.site.utils.exceptions.ServiceConflictException;
import org.example.site.utils.exceptions.ServiceException;
import org.example.site.utils.exceptions.ServiceNotCorrectInputException;

public class CountryCityInfrService {


    public void addWithCheck(String country, String city) throws Exception{
        check(country,city);
        boolean added = MyContext.countryCityRepoJDBCImpl.add(Country.valueOf(country), Country.City.valueOf(city));
        if(!added){
            throw new ServiceConflictException("not added");
        }
    }


    public boolean check(String country, String city) throws Exception{
        try{
            Country c = Country.valueOf(country);
        } catch (Exception e) {
            throw new ServiceNotCorrectInputException("incorrect country", e);
        }

        try{
            Country.City.valueOf(city);
        } catch (Exception e){
            throw new ServiceNotCorrectInputException("incorrect city", e);
        }

        return true;
    }


}
