package org.example.site.infrastructurePartOfSite.models;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class PlaceInFlight {

    @NonNull
    private Flight flight;
    @NonNull
    private Integer place;

    @Builder
    public PlaceInFlight(Flight flight,
                         Integer place) {
        this.flight = flight;
        this.place = place;
    }
}
