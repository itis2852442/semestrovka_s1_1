create extension if not exists "uuid-ossp";

-- create type role_worker as enum ('pilot','security');

--create type role_site_user as enum ('simple_customer','admin','router','super');

-- create type role_site_worker as enum ('admin','router','super');
-- create type role_customer as enum ('simpleCustomer');


-- fixme Question как делать правильней? одну большую таблицу в которой могут хранится и site_customer и site_worker
--  или две таблицы site_customer и site_worker?
--  если одна большая таблица - тогда,например, ticket будет ссылаться просто на таблицу site_user, а в таблице site_user может быть site_worker, у которого ticket е может быть.
--  в таком случае - как наложить ограничение на foreign key?
--
create table site_customer
(
    token            uuid PRIMARY KEY   not null DEFAULT uuid_generate_v4(),
    name             varchar(50)        not null,
    surname          varchar(50)        not null,
    patronymic       varchar(50)        not null,
    age    int               not null,
--     gender           varchar(20)        not null, --fixme enum

    password         varchar            not null,
    banned           bool               not null,
    role             varchar      not null,

    passport_country varchar            not null,--fixme enum
    passport_digits  varchar(50)        not null,
    constraint passportCountry_passportDigits_unq unique (passport_country, passport_digits),

    phone_number     varchar(30) unique not null,
    unique (phone_number),
    email            varchar(50) unique not null,
    unique (email)


);

create table site_worker
(
    token         uuid PRIMARY KEY not null DEFAULT uuid_generate_v4(),
    name          varchar(50)      not null,
    surname       varchar(50)      not null,
    patronymic    varchar(50)      not null,
    age int             not null,
--     gender        varchar(20)      not null, --fixme enum

    password      varchar          not null,
    banned        bool             not null,
    role          varchar not null,

    phone_number  varchar(30)      not null,
    unique (phone_number),
    email            varchar(50) unique not null,
    unique (email)


);


/*create table active_seances
(
    id_device           varchar unique not null,
    site_customer_token uuid,
    owner               role_site_user not null,
    foreign key (site_customer_token) references site_customer (token),
    site_worker_token   uuid,
    foreign key (site_worker_token) references site_worker (token),
    constraint owner_constr check (
            (site_customer_token IS NOT NULL and site_worker_token is null and owner in ('simple_customer')) or
            (site_customer_token is null and site_worker_token is not null and owner in ('admin', 'router', 'super')) )
    --fixme site_customer_token и site_worker_token должны быть взаимно null'o-обратны: если один null, другой - не null.
    -- если была бы одна большая таблица site_user, то здесь был бы только один fkey
);*/


/* второй вариант: одна большая таблица
create table site_user
(
    token            uuid PRIMARY KEY   not null,
    name             varchar(50)        not null,
    surname          varchar(50)        not null,
    patronymic       varchar(50)        not null,
    year_of_birth    date               not null,
    gender           varchar(20)        not null,

    password         varchar            not null,
    logined          bool               not null,
    banned           bool               not null,
    role role_site_user not null,

    passport_country varchar,
    passport_digits  varchar(50),
    constraint passportCountry_passportDigits_unq unique (passport_country, passport_digits),
    constraint passport_nn check ( passport_country is not null and passport_digits is not null and role in ('simple_customer')),

    phone_number     varchar(30) unique not null,
    email            varchar(50) unique not null


);

create table active_seances
(
    id_device           varchar unique not null,
    site_user_token uuid not null,
    foreign key (site_user_token) references site_user (token)
);*/

create table distro_model_for_aircrafts
(
    distro varchar not null,
    model    varchar not null,
    unique (distro, model)
);

/*
todo
некоторые таблицы могли бы быть в другой базе данных,
к которой бы обращались другие репозитории,
которые бы находились в отдельном микросервисе,
к которому бы обращались сервисы в этом приложении
*/
create table aircraft
(
    on_board_number varchar(10) PRIMARY KEY not null,

    distro          varchar                 not null,
    model           varchar                 not null,
    foreign key (distro, model) references distro_model_for_aircrafts(distro, model),
    age    int,

    max_capacity    int                     not null,
    amount_of_seats int not null

--     seats json not null,
--     schema json not null
);



create table places_in_aircraft
(
    aircraft_on_board_number varchar(10) not null,
    foreign key (aircraft_on_board_number) references aircraft (on_board_number),
    place int not null,
    unique (aircraft_on_board_number, place),
    isWindow bool not null,
    comfort boolean not null
);

create table country_city_for_airports
(
    country varchar not null,
    city    varchar not null,
    unique (country, city)
);

create table airport
(
    id      uuid primary key not null DEFAULT uuid_generate_v4(),
    name varchar not null,
    country varchar          not null,
    city    varchar          not null,
    foreign key (country, city) references country_city_for_airports (country, city),
    unique (name, country, city)

);

/*create table airport_worker
(
    id                      uuid PRIMARY KEY not null DEFAULT uuid_generate_v4(),
    name                    varchar(50)      not null,
    surname                 varchar(50)      not null,
    patronymic              varchar(50)      not null,
    year_of_birth           date             not null,
    gender                  varchar(20)      not null, --fixme enum

    airport_id              uuid             not null,
    foreign key (airport_id) references airport (id),
    year_of_entry_into_work date             not null,
    position                role_worker      not null,

    phone_number            varchar(30)      not null,
    email                   varchar(50)      not null
);*/



-- create table aircraft_seats
-- (
--     on_board_number varchar(10),
--     foreign key (on_board_number) references aircraft (on_board_number),
--     seats json not null,
--     schema json not null
--
-- );


--path = маршрут (ex. москва - сочи)
create table path
(
    id                   uuid PRIMARY KEY not null DEFAULT uuid_generate_v4(),
    departure_airport_id uuid             not null,
    foreign key (departure_airport_id) references airport (id),
    arrival_airport_id   uuid             not null,
    foreign key (arrival_airport_id) references airport (id)
);

-- на какие маршруты могут летать самолеты
create table aircraft_path
(
    on_board_number varchar not null,
    foreign key (on_board_number) references aircraft (on_board_number),
    id_path         uuid    not null,
    foreign key (id_path) references path (id),
    constraint onBoardNumber_idPath_unq unique (on_board_number, id_path)
);

-- таблица в которую добавляют трансферы, после выполнения трансферы удаляются из этой таблицы
create table flight
(
    id                       uuid PRIMARY KEY      not null DEFAULT uuid_generate_v4(),
    --id_crew serial unique not null,
    id_path                  uuid                  not null,
    aircraft_on_board_number varchar               not null,
    foreign key (aircraft_on_board_number, id_path) references aircraft_path (on_board_number, id_path),
    unique (id, aircraft_on_board_number),
    -- check on_board_number and id_path in path_aircraft

    price                    int CHECK (price > 0) not null,
--     departure_date           date                  not null,
--     departure_time           time                  not null
    departure_month int check ( 1<=departure_month And departure_month<=12 ) not null
);

--
-- create table crew_people (
--     id_crew int not null,
--     id_employee int not null,
--     foreign key (id_crew) references flight (id_crew) ,
--     foreign key (id_employee) references airport_worker (id)
-- );

create table ticket
(
    id               uuid PRIMARY KEY not null DEFAULT uuid_generate_v4(),
    id_site_customer uuid             not null,
    foreign key (id_site_customer) references site_customer (token),
    id_flight        uuid             not null,
    foreign key (id_flight) references flight(id),
    aircraft_on_board_number varchar(10) not null,
    foreign key (id_flight, aircraft_on_board_number) references flight(id, aircraft_on_board_number),
    place int not null,
    foreign key (aircraft_on_board_number, place) references places_in_aircraft(aircraft_on_board_number,place),
    price            int              not null,
    --foreign key (id_flight, price) references flight(id, price),
    --sale_in_percents int default 0 not null check ( 0 <==   sale_in_percents <= 90 ),
    --todo price with sales
    purchase_date    date             not null,
    purchase_time    time             not null

);








