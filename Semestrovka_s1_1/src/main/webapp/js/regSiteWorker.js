async function sendAjaxToServerToRegisterSiteWorker(url,
                                                    email,
                                                    password,
                                                    siteWorkerFirstName,
                                                    siteWorkerLastName,
                                                    siteWorkerPatronymic,
                                                    siteWorkerAge,
                                                    siteWorkerPhoneNumber,
                                                    siteWorkerRole
){
    url = new URL(url);
    url.searchParams.append('email',email);
    url.searchParams.append('password',password);
    url.searchParams.append('firstName',siteWorkerFirstName);
    url.searchParams.append('passportCountry',siteWorkerPassportCountry);
    url.searchParams.append('lastName',siteWorkerLastName);
    url.searchParams.append('patronymic',siteWorkerPatronymic);
    url.searchParams.append('age',siteWorkerAge);
    url.searchParams.append('phoneNumber',siteWorkerPhoneNumber);
    url.searchParams.append('role',siteWorkerRole);

//todo
    if(!email.split("@").size===2){
        alert("js: incorrect email")
        return;
    }

    if(email.length<3){
        alert("js: incorrect email")
        return;
    }




    const response= await fetch(url, {method: 'POST'});
    if(response.status===500){
        alert("js: error on server")
    }
    else if(response.status===400){
        response.text()
            .then(res=>{
                alert("server: " + res)
            })
            .catch(err=>alert("js: error: " + err))
    }
    else if(response.status===409){
        response.text()
            .then(res=>{
                alert("server: " + res)
            })
            .catch(err=>alert("js: error: " + err))
    }
    else if(response.status===200){
        alert("js: server should redirect")
        window.location.replace("http://localhost:8080/infr/login")
    }
    else{
        alert("server: status " + response.status)
    }

}