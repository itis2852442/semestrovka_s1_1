async function sendAjaxToServerToRegisterSiteCustomer(url,
                                                email,
                                                password,
                                                siteCustomerPassportCountry,
                                                siteCustomerPassportDigits,
                                                siteCustomerFirstName,
                                                siteCustomerLastName,
                                                siteCustomerPatronymic,
                                                siteCustomerAge,
                                                siteCustomerPhoneNumber
                                                ){
    url = new URL(url);
    url.searchParams.append('email',email);
    url.searchParams.append('password',password);
    url.searchParams.append('passportCountry',siteCustomerPassportCountry);
    url.searchParams.append('passportDigits',siteCustomerPassportDigits);
    url.searchParams.append('firstName',siteCustomerFirstName);
    url.searchParams.append('passportCountry',siteCustomerPassportCountry);
    url.searchParams.append('lastName',siteCustomerLastName);
    url.searchParams.append('patronymic',siteCustomerPatronymic);
    url.searchParams.append('age',siteCustomerAge);
    url.searchParams.append('phoneNumber',siteCustomerPhoneNumber);

//todo
    if(!email.split("@").size===2){
        alert("js: incorrect email")
        return;
    }

    if(email.length<3){
        alert("js: incorrect email")
        return;
    }




    const response= await fetch(url, {method: 'POST'});
    if(response.status===500){
        alert("js: error on server")
    }
    else if(response.status===400){
        response.text()
            .then(res=>{
                alert("server: " + res)
            })
            .catch(err=>alert("js: error: " + err))
    }
    else if(response.status===409){
        response.text()
            .then(res=>{
                alert("server: " + res)
            })
            .catch(err=>alert("js: error: " + err))
    }
    else if(response.status===200){
        alert("js: server should redirect")
        window.location.replace("http://localhost:8080/login")
    }
    else{
        alert("server: status " + response.status)
    }

}