var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost:8080/infr/system/country', true);

xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        // alert(response[0]['city']);
        // alert(response.toString())
        // for(var key in response)
        //     console.log(key + ": " + response[key]);
        // console.dir(response)
        var avWordsCountry = [];
        for(var country of response){
            avWordsCountry.push(country)
        }
        $( function() {
            $( "#country" ).autocomplete({
                source: avWordsCountry
            });
        } );
    }
};

xhr.send();


var xhr2 = new XMLHttpRequest();
xhr2.open('GET', 'http://localhost:8080/infr/system/city', true);

xhr2.onreadystatechange = function() {
    if (xhr2.readyState === 4 && xhr2.status === 200) {
        var response = JSON.parse(xhr2.responseText);
        // alert(response[0]['city']);
        // alert(response.toString())
        // for(var key in response)
        //     console.log(key + ": " + response[key]);
        // console.dir(response)
        var avWordsCity = [];
        for(var city of response){
            avWordsCity.push(city)
        }
        $( function() {
            $( "#city" ).autocomplete({
                source: avWordsCity
            });
        } );
    }
};

xhr2.send();