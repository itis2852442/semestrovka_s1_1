// async function r(url = ""){
//     const response= await fetch(url, {method: 'GET'});
//     const res = await response.json();
//     return res;
// }
//
// r("http://localhost:8080/infr/system/airports").then(avWords=>{
//     alert(avWords)
//     $( function() {
//         $( "#tags" ).autocomplete({
//             source: avWords
//         });
//     } );
// })
function ajaxAutoComplete(url,sel) {


    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            // alert(response[0]['city']);
            // alert(response.toString())
            // for(var key in response)
            //     console.log(key + ": " + response[key]);
            // console.dir(response)
            var avCountriesCities = [];
            for (var countryCity of response) {
                avCountriesCities.push(countryCity)
            }
            $(function () {
                $(sel).autocomplete({
                    source: avCountriesCities
                });
            });
        }
    };

    xhr.send();
}
