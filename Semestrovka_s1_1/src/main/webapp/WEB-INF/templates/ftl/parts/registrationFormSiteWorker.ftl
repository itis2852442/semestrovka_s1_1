<div class="registrationFromForSiteWorkerWithAjax" id="registrationFromForSiteWorkerWithAjax">
    <form>
        email: <input type="text" name="email" id="siteWorkerEmail" required/>
        password: <input type="text" name="password" id="siteWorkerPassword" required/>

        firstName: <input type="text" name="firstName" id="siteWorkerFirstName" required/>
        lastName: <input type="text" name="lastName" id="siteWorkerLastName" required/>
        patronymic: <input type="text" name="patronymic" id="siteWorkerPatronymic" required/>
        age: <input type="text" name="age" id="siteWorkerAge" required/>
        phoneNumber: <input type="text" name="phoneNumber" id="siteWorkerPhoneNumber" required/>
        role: <input type="text" name="role" id="siteWorkerRole" required/>
        <#--        <input type="submit"/>-->
    </form>

    <button onclick="sendAjaxToServerToRegisterSiteWorker('http://localhost:8080/infr/superr/regSiteWorker',
        document.getElementById('siteWorkerEmail').value,
        document.getElementById('siteWorkerPassword').value,
        document.getElementById('siteWorkerFirstName').value,
        document.getElementById('siteWorkerLastName').value,
        document.getElementById('siteWorkerPatronymic').value,
        document.getElementById('siteWorkerAge').value,
        document.getElementById('siteWorkerPhoneNumber').value,
        document.getElementById('siteWorkerRole').value,
    )">register_site_worker</button>


</div>

