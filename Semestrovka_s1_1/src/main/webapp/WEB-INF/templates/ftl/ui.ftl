<#import 'ui.ftl' as ui>

<#macro ButtonToGoToLink href buttonName>
    <a href=${href}>
        <button>${buttonName}</button>
    </a>
</#macro>

<#macro form1Name action method inputType inputName >
    <form action=${action} method=${method}>
        <input type=${inputType} name=${inputName}/>
    </form>
</#macro>

<#macro form1Value action method inputType inputValue>
    <form action=${action} method=${method}>
        <input type=${inputType} value=${inputValue} />
    </form>
</#macro>

<#macro form2 action method value>
    <form action=${action} method=${method}>
        email: <input type="text" name="email" required/>
        password: <input type="text" name="password" required/>
        PassportCountry: <input type="text" name="passportCountry" required/>
        passportDigits: <input type="text" name="passportDigits" required/>

        firstName: <input type="text" name="firstName" required/>
        lastName: <input type="text" name="lastName" required/>
        patronymic: <input type="text" name="patronymic" required/>
        age: <input type="text" name="age" required/>
        phoneNumber: <input type="text" name="phoneNumber" required/>
        <input type="submit" value=${value}/>
    </form>
</#macro>

<#macro form2Name action method name>
    <form action=${action} method=${method}>
        <input type="text" name="login" required/>
        <input type="text" name="password" required/>
        <input type="submit" name=${name}/>
    </form>
</#macro>

<#macro form2Inc action method>
    <form action=${action} method=${method}>
        <input type="text" name="login" required/>
        <input type="text" name="password" required/>
        <p>incorrect login and password</p>
        <input type="submit" value="login"/>
    </form>
</#macro>


